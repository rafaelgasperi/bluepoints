<?php
/**
 * Created by PhpStorm.
 * User: rafael
 * Date: 12/05/14
 * Time: 12:29 PM
 */

require "auth/secure.php";

include_once "lang.php";
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering

require_once("db/db.php");

$sql = 'SELECT u.id,u.nombre,u.apellido,u.pais,u.pais_ip,r.id "idregistro",r.puntos_iniciales,r.puntos_restantes, ';
$sql .= 'm.puntos as "panama",m.salvador,m.nicaragua,m.costarica,m.ecuador,m.dominicana,m.honduras,m.jamaica,m.trinidad ';
$sql .= 'FROM users u ';
$sql .= 'LEFT JOIN registro r ON u.id = r.user_id ';
$sql .= 'LEFT JOIN modelo_registro mr ON r.id = mr.registro_id ';
$sql .= 'LEFT JOIN modelos m ON mr.modelo_id = m.id ';
$sql .= 'where u.pais <> u.pais_ip ';
$sql .= 'and if(u.pais = "T&T" and u.pais_ip = "TT",false,true) ';
$sql .= 'and if(u.pais = "ECU" and u.pais_ip = "EC",false,true) ';
$sql .= 'and if(u.pais = "RD"  and u.pais_ip = "DO",false,true) ';
$sql .= 'and if(u.pais = "HON" and u.pais_ip = "HN",false,true) ';
$sql .= 'and if(u.pais = "JAM" and u.pais_ip = "JM",false,true) ';
$sql .= 'group by r.id ';
$sql .= 'order by u.pais_ip ';

$result = mysql_query($sql);
while($row = mysql_fetch_array($result))
{
    echo "----------- REGISTRO ".$row['idregistro']." ------------------"."<br/>";
    echo "<pre>";
    print_r($row);
    echo "</pre>";

    $puntos_pais = 0;
    $pais = '';
    switch ($row['pais_ip'])
    {
        case 'PA':$puntos_pais = $row['panama'];$pais='PA';$campo='puntos';break;
        case 'CR':$puntos_pais = $row['costarica'];$pais='CR';$campo='costarica';break;
        case 'DO':$puntos_pais = $row['dominicana'];$pais='RD';$campo='dominicana';break;
        case 'GT':$puntos_pais = $row['salvador'];$pais='SV';$campo='salvador';break;
        case 'HN':$puntos_pais = $row['honduras'];$pais='HON';$campo='honduras';break;
        case 'JM':$puntos_pais = $row['jamaica'];$pais='JAM';$campo='jamaica';break;
        case 'NI':$puntos_pais = $row['nicaragua'];$pais='NI';$campo='nicaragua';break;
        case 'SV':$puntos_pais = $row['salvador'];$pais='SV';$campo='salvador';break;
        case 'US':$puntos_pais = $row['ecuador'];$pais='ECU';$campo='ecuador';break;
        case 'XX':$puntos_pais = $row['salvador'];$pais='SV';$campo='salvador';break;
        case 'TT':$puntos_pais = $row['trinidad'];$pais='T&T';$campo='trinidad';break;
        default:$puntos_pais = $row['panama'];$pais='PA';break;
    }

    $sql2 = 'UPDATE users SET pais = "'.$pais.'" WHERE id = "'.$row['id'].'"';

    $result2 = mysql_query($sql2);

    if($result2)
    {
        $sql3 = 'SELECT r.id,r.puntos_iniciales,m.id,m.modelo,m.'.$campo.' FROM registro r ';
        $sql3 .= 'LEFT JOIN modelo_registro mr ON r.id = mr.registro_id ';
        $sql3 .= 'LEFT JOIN modelos m ON mr.modelo_id = m.id ';
        $sql3 .= 'WHERE r.id = "'.$row['idregistro'].'"';
        $result3 = mysql_query($sql3);
        $puntos_factura = 0;

        echo "----------- MODELOS DEL REGISTRO ------------------"."<br/>";
        while($row2 = mysql_fetch_array($result3))
        {

            echo "<pre>";
            print_r($row2);
            echo "</pre>";

          $puntos_factura += $row2[$campo];
        }

        echo "Puntos Totales :".$puntos_factura."<br/>";

        $sql4 = 'UPDATE registro SET puntos_iniciales = "'.$puntos_factura.'",puntos_restantes = "'.$puntos_factura.'" WHERE id = "'.$row['idregistro'].'" ';
        $result4 = mysql_query($sql4);

        if($result4)
        {
            echo "El registro <strong>".$row['idregistro']."</strong> fue actualizado de <strong>".$row['puntos_iniciales']."</strong> a <strong>".$puntos_factura."</strong><br/><br/>";
        }
        else
        {
            echo "Hubo un error al actualizar el registro ".$row['idregistro']."<br/>";
        }
    }

    echo "------------------------------------------------------------------------------"."<br/><br/>";

}