<?php
if (isset($_SESSION['lang']))
{
    $lang = $_SESSION['lang'];
}
else
{
    $lang = "es";
}
switch ($lang)
{
    case 'en':
        $lang_file = 'lang.en.php';
        break;
    case 'es':
        $lang_file = 'lang.es.php';
        break;
    default:
        $lang_file = 'lang.es.php';
}

include_once 'lang/'.$lang_file;
?>