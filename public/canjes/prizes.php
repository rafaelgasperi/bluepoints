<?php
/**
 * Created by PhpStorm.
 * User: rafael
 * Date: 12/03/14
 * Time: 10:28 AM
 */

require "auth/secure.php";
include_once "lang.php";
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering

require_once("db/db.php");

$sql .= "SELECT * FROM bluepoints.premios ";

//
if ($_SESSION['user']['pais'] != "TODOS")
{
    $sql .= "where pais_id = '".$_SESSION['user']['pais']."'";
}

$result = mysql_query($sql);
?>
<!DOCTYPE html>
<html>
<head lang="en">
    <link rel="stylesheet" href="../assets/vendor/bootstrap/dist/css/bootstrap.min.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="custom.css">
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="http://cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="http://cdn.datatables.net/plug-ins/9dcbecd42ad/integration/bootstrap/3/dataTables.bootstrap.js"></script>
    <script src="../assets/js/respond.js"></script>
    <meta charset="UTF-8">
    <title></title>
</head>
<body  class="container">
<p></p>

<div class="text-right">
    <a class="btn btn-warning btn-sm" href="../canjes/list.php"><i class="fa fa-list"></i> <?php echo $lang['TRANSACTION_LIST_LIST_TITLE']; ?></a>
    <a class="btn btn-success btn-sm" href="../canjes/auth/logout.php"><i class="icon-signout"></i> <?php echo $lang['TRANSACTION_LIST_LOGOUT_TITLE']; ?></a>
</div>
<p></p>
<div class="panel panel-primary">

    <div class="panel-heading">
        <h3 class="panel-title"><?php echo $lang['PRIZES_LIST_TABLE_TITLE']; ?></h3>
    </div>
    <div class="panel-body">
        <?php /*if (count($result) > 0){ */?>
        <table id="myTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th data-field="id"          data-sortable="true"><?php echo $lang['PRIZES_LIST_TABLE_HEADER_ID']; ?></th>
                <th data-field="premio"      data-sortable="true"><?php echo $lang['PRIZES_LIST_TABLE_HEADER_PRIZE']; ?></th>
                <th data-field="puntos"      data-sortable="true"><?php echo $lang['PRIZES_LIST_TABLE_HEADER_POINTS']; ?></th>
                <th data-field="disponibles" data-sortable="true"><?php echo $lang['PRIZES_LIST_TABLE_HEADER_QUANTITY']; ?></th>
                <th data-field="pais"        data-sortable="true"><?php echo $lang['PRIZES_LIST_TABLE_HEADER_COUNTRY']; ?></th>
            </tr>
            </thead>
            <tbody>
            <?php while($row = mysql_fetch_array($result)) { ?>
                <tr>
                    <td><?php echo $row['id']; ?></td>
                    <td><?php echo $row['premio']; ?></td>
                    <td><?php echo $row['puntos']; ?></td>
                    <td><?php echo $row['disponible']; ?></td>
                    <td><?php echo $row['pais_id']; ?></td>

                </tr>
            <?php } ?>
            </tbody>

        </table>
        <?php /*} else{ ?>
            <h2>No hay ninguna transaccion para su pais.</h2>
        <?php} */?>
    </div>
</div>

<script>
    $(document).ready(function(){
        var show = "<?php echo $lang['TRANSACTION_LIST_TABLE_SHOW_TITLE']; ?>";
        var rpp = "<?php echo $lang['TRANSACTION_LIST_TABLE_ENTRIES_TITLE']; ?>";
        var NR = "<?php echo $lang['TRANSACTION_LIST_TABLE_ZERO_RECORD']; ?>";
        var IE = "<?php echo $lang['TRANSACTION_LIST_TABLE_INFO_EMPTY']; ?>";
        var ser = "<?php echo $lang['TRANSACTION_LIST_TABLE_SEARCH_TITLE']; ?>";

        $('#myTable').dataTable( {
            "lengthMenu": [[20, 30, 50, -1], [20, 30, 40, "All"]],
            "language": {
                "lengthMenu": show+" _MENU_ "+rpp,
                "zeroRecords": NR,
                "search": ser,
                "paginate_button previous": "PREVIOS",
                "info": "Showing page _PAGE_ of _PAGES_",
                "infoEmpty": IE,
                "infoFiltered": "(filtered from _MAX_ total records)"
            }

        } );
    });
</script>
</body>
</html>