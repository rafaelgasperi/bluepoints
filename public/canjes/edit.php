<?php
/**
 * Created by PhpStorm.
 * User: rafael
 * Date: 11/24/14
 * Time: 02:31 PM
 */
require "auth/secure.php";
include_once "lang.php";

if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering

require_once("db/db.php");
$message = "";
$message2 = "";
$id = $_GET['id'];
if(empty($id))
{
    die("Transaccion Invalida");
}
if ($_POST)
{
    // SETEAR TODAS LAS FACTURAS A 0 ANTES DE INSERTAR
    $result7 = mysql_query("DELETE FROM facturas_validadas WHERE id_transaccion = '".$id."'");
    $total_checks = 0;   
     
    foreach($_POST as $key => $value)
    {
     
        $pieces = explode("_", $key);

        if ($pieces[0] == "checkbox")
        {
            if($row3['id'])
            {
                $sql4 = "UPDATE facturas_validadas SET canjeado = 1,username_report = '".$_SESSION['user']['username']."' WHERE id_registro = '".$pieces[1]."'";
                $result4 = mysql_query($sql4);
            }
            else
            {
                $sql4 = "INSERT INTO facturas_validadas (id_registro,canjeado,id_transaccion,username_report,fecha_registro) values ('".$pieces[1]."',1,'".$id."','".$_SESSION['user']['username']."',NOW()) ";
                $result4 = mysql_query($sql4);
            }
            $total_checks++;

            if($result4)
            {
                $message ='<div class="alert alert-success" role="alert">Guardado con exito</div>';
            }
            else
            {
                $message ='<div class="alert alert-danger" role="alert">Hubo un error guardando.Intente nuevamente mas tarde</div>';
            }

        }
    }   
    

    if($total_checks == $_POST['total_facturas'])
    {
        $result8 = mysql_query("UPDATE transaccion SET nota = 4 WHERE id = '".$id."'");

        $result9 = mysql_query('INSERT INTO log_transacciones (username,id_transaccion,fecha,estatus) VALUES ("'.$_SESSION['user']['username'].'","'.$id.'",NOW(),"CANJEADO") ');

        $message2 ='<div class="alert alert-success" role="alert">La transaccion #'.$id.' ha cambiado su estatus a Canjeado.</div>';
    }
    else
    {
        $result3 = mysql_query("SELECT * FROM transaccion WHERE id = '".$id."' ");
        $row3 = mysql_fetch_array($result3);

        if($row3['nota'] == 4)
        {
            $result8 = mysql_query("UPDATE transaccion SET nota = 2 WHERE id = '".$id."'");
            $message2 ='<div class="alert alert-success" role="alert">La transaccion #'.$id.' ha cambiado su estatus a Aprobado.</div>';
        }

    }
}

$sql .= "SELECT t.id,u.nombre,u.apellido,u.identidad,u.direccion,u.email,u.telefono,re.nombre_tienda,p.premio,t.created_at,t.puntos_usados,t.nota FROM transaccion t ";
$sql .= "inner join users u on t.user_id = u.id ";
$sql .= "left join premios p on t.id_premio = p.id ";
$sql .= "inner join registro re on re.user_id = u.id ";
$sql .= "where t.id = '".$id."' ";

$result = mysql_query($sql);
$row = mysql_fetch_array($result);

?>
<!DOCTYPE html>
<html>
<head lang="en">
    <link rel="stylesheet" href="../assets/vendor/bootstrap/dist/css/bootstrap.min.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="custom.css">
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="http://cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="http://cdn.datatables.net/plug-ins/9dcbecd42ad/integration/bootstrap/3/dataTables.bootstrap.js"></script>
    <script src="../assets/js/respond.js"></script>
    <script src="../assets/vendor/bootstrap/dist/js/bootstrap.min.js"></script>


    <meta charset="UTF-8">
    <title></title>
</head>
<body  class="container">
<p></p>
<div class="text-right">
    <a class="btn btn-success btn-sm" href="../canjes/list.php"><i class="fa fa-backward"></i> <?php echo $lang['TRANSACTION_EDIT_BUTTON_BACK_TITLE']; ?></a>
</div>
<p></p>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo $lang['TRANSACTION_EDIT_TABLE_TITLE']; ?> #<?php echo $row['id']; ?></h3>
    </div>
    <div class="panel-body">
        <dl class="dl-horizontal">
            <div class="col-md-6">
                <dt><?php echo $lang['TRANSACTION_EDIT_DATE_TITLE']; ?> :</dt>
                <dd><?php $date = new DateTime($row['created_at']); echo $date->format('d-m-Y');?></dd>
                <dt><?php echo $lang['TRANSACTION_EDIT_HOUR_TITLE']; ?> :</dt>
                <dd><?php echo $date->format('H:i:s');?></dd>
                <dt><?php echo $lang['TRANSACTION_EDIT_POINTS_TITLE']; ?> :</dt>
                <dd><?php echo $row['puntos_usados'];?></dd>
                <dt><?php echo $lang['TRANSACTION_EDIT_STATUS_TITLE']; ?> :</dt>
                <dd><?php if($row['nota'] == 1 ){ echo "En Espera";}if($row['nota'] == 2 ){ echo "Aprobado";}if($row['nota'] == 3 ){ echo "Anulada";}if($row['nota'] == 4 ){ echo "Canjeado";}?></dd>
                <dt><?php echo $lang['TRANSACTION_EDIT_PRIZE_TITLE']; ?> :</dt>
                <dd><?php echo $row['premio'];?></dd>
                <dt><?php echo $lang['TRANSACTION_EDIT_NOTE_TITLE']; ?> :</dt>
                <dd><?php echo $row['premio'];?></dd>

            </div>
            <div class="col-md-6">
                <dt><?php echo $lang['TRANSACTION_EDIT_CHANGED_BY_ITLE']; ?> :</dt>
                <dd><?php echo $row['nombre']." ".$row['apellido'];?></dd>
                <dt><?php echo $lang['TRANSACTION_EDIT_ID_NUMBER_TITLE']; ?> :</dt>
                <dd><?php echo $row['identidad'];?></dd>
                <dt><?php echo $lang['TRANSACTION_EDIT_PHONE_TITLE']; ?> :</dt>
                <dd><?php echo $row['telefono'];?></dd>
                <dt><?php echo $lang['TRANSACTION_EDIT_EMAIL_TITLE']; ?> :</dt>
                <dd><?php echo $row['email'];?></dd>
                <dt><?php echo $lang['TRANSACTION_EDIT_ADDRESS_TITLE']; ?> :</dt>
                <dd><?php echo $row['direccion'];?></dd>
                <dt><?php echo $lang['TRANSACTION_EDIT_STORENAME_TITLE']; ?> :</dt>
                <dd><?php echo $row['nombre_tienda'];?></dd>
            </div>

        </dl>
        <?php if ($row['nota'] != 3){ ?>
                <div class="text-right">
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal">
                        <i class="fa fa-eraser"></i> <?php echo $lang['TRANSACTION_EDIT_BUTTON_CANCEL_TITLE']; ?>
                    </button>
                </div>
        <?php } ?>
    </div>

</div>
<div id="modal-results" ></div>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo $lang['TRANSACTION_EDIT_TABLE_INVOICE_TRANSACTION_TITLE']; ?></h3>
    </div>
    <p></p>

    <div class="panel-body">
        <?php echo $message; ?>
        <p></p>
        <?php echo $message2; ?>
        <p></p>
        <form method="post" action="">
            <div class="text-right">
                <?php if ($row['nota'] != 3){ ?>
                <button id="submit1" type="submit" class="btn btn-success btn-sm"><i class="fa fa-save"></i> <?php echo $lang['TRANSACTION_EDIT_BUTTON_SAVE_TITLE']; ?></button>
               <?php } ?>
                <p></p>
            </div>
            <table id="myTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>

                    <th data-field="factura"  data-sortable="true"><?php echo $lang['TRANSACTION_EDIT_TABLE_INVOICE_TITLE']; ?></th>
                    <th data-field="fecha"    data-sortable="true"><?php echo $lang['TRANSACTION_EDIT_TABLE_DATE_TITLE']; ?></th>
                    <th data-field="codigo"   data-sortable="true"><?php echo $lang['TRANSACTION_EDIT_TABLE_CODE_TITLE']; ?></th>
                    <th data-field="modelo"   data-sortable="true"><?php echo $lang['TRANSACTION_EDIT_TABLE_MODEL_TITLE']; ?></th>
                    <th data-field="accion"   data-sortable="false"><?php echo $lang['TRANSACTION_EDIT_TABLE_VALIDATE_TITLE']; ?></th>
                </tr>
                </thead>
                <tbody>
                <?php $sql2 .="SELECT r.id,r.factura,r.fecha,r.nombre_tienda,c.codigo,m.modelo ";
                $sql2 .="FROM registro_transaccion rt ";
                $sql2 .="LEFT JOIN registro r ON rt.registro_id = r.id ";
                $sql2 .="LEFT JOIN codigos c ON r.codigo_id = c.id ";
                $sql2 .="LEFT JOIN modelo_registro mr ON r.id = mr.registro_id ";
                $sql2 .="LEFT JOIN modelos m ON mr.modelo_id = m.id ";
                $sql2 .="WHERE transaccion_id = '".$row['id']."'";

                $result2 = mysql_query($sql2);
                $i=0;
                $r=1;
                ?>
                <?php while($row2 = mysql_fetch_array($result2)) { ?>
                    <tr>

                        <td><?php echo $row2['factura']; ?></td>
                        <td><?php echo $row2['fecha'];   ?></td>
                        <td><?php echo $row2['codigo'];  ?></td>
                        <td><?php echo $row2['modelo'];  ?></td>
                       
                        <td>
			 <?php
                        $result5 = "";
                        $row5 = "";
                        $sql5 = "SELECT * FROM facturas_validadas WHERE id_registro = '".$row2['id']."'";
                        $result5 = mysql_query($sql5);
                        $row5 = mysql_fetch_array($result5);
			$i++;                        
                        ?>
 <input name="checkbox_<?php echo $r;?>" id="checkbox_<?php echo $r;?>" type="checkbox" <?php if($row5['canjeado']){ echo 'checked'; } ?>>
                        </td>
                        
                    </tr>
                <?php 
		$r++;
		} ?>
                </tbody>
            </table>
            <input type="hidden" name="total_facturas" value="<?php echo $i; ?>">
        </form>
        <!-- modal form -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title" id="myModalLabel"><b><?php echo $lang['TRANSACTION_EDIT_POPUP_TITLE']; ?> - # <?= $row['id']; ?></b></h3>
                    </div>
                    <div class="modal-body">
                        <form class="test" role="form">
                            <div class="form-group">
                                <input type="hidden" name="id_trans" value="<?php echo $id; ?>">
                                <textarea name="comentarios" class="form-control" rows="3" placeholder="<?php echo $lang['TRANSACTION_EDIT_POPUP_COMMENT_TITLE']; ?>" required></textarea>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $lang['TRANSACTION_EDIT_POPUP_CLOSE_TITLE']; ?></button>
                        <button type="button" class="btn btn-primary" id="update"><?php echo $lang['TRANSACTION_EDIT_POPUP_SEND_TITLE']; ?></button>
                    </div>
                </div>
            </div>

        <script>
            $(document).ready(function(){

                $("button#update").click(function(){
                    $.ajax({
                        type: "POST",
                        url: "anulartransaccion.php",
                        data: $('form.test').serialize(),
                        success: function(msg){
                            $("#modal-results").html(msg)
                            $("#myModal").modal('hide');
                        },
                        error: function(){
                            alert("failure");
                        }
                    });
                });

                $('#myTable').dataTable( {
                    "lengthMenu": [[-1], ["All"]]
                });

                window.setTimeout(function() { $(".alert").alert('close'); }, 5000);


            });

        </script>
    </div>
</div>
</body>
</html>
