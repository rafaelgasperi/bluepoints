$(function() {
    $('#premios2').carouFredSel({
		responsive: true,
		auto:true,
		synchronise: ['#premios3', false],
		prev: '#prev2',
		next: '#next2',
		width: '100%',
		scroll: 1000,
		items: {
			width: 180,
			visible: {
				min: 1,
				max: 3
			}
		}
	});

	$('#premios3').carouFredSel({
		responsive: true,
		auto:true,
		width: '100%',
		scroll: 1000,
		items: {
			width: 300,
			visible: {
				min: 1,
				max: 3
			}
		}
	});

});