$( document ).ready(function() {
  value = $('#premio').val();
  cant = $('#cantidad').val();
  cambiarValor(value);

  $('#plus').click(function(){
    if($('#cantidad').val() != 0)
      $('#cantidad').val(parseInt($('#cantidad').val()) + 1);
    else
      $('#cantidad').val(1);
  });

  $('#less').click(function(){
    if($('#cantidad').val() != 1)
      $('#cantidad').val(parseInt($('#cantidad').val()) - 1);
  });

  $('#premio').each(function() {
     var elem = $(this);
     // Save current value of element
     elem.data('oldVal', elem.val());
     // Look for changes in the value
     elem.on("change", function(event){
        // If value has changed...
        if (elem.data('oldVal') != elem.val()) {
         // Updated stored value
         elem.data('oldVal', elem.val());
         // Do action
         if (/^[\d]+$/.exec(elem.val())) {
          value = elem.val();
          cambiarValor(value);
        }
       }
     });
   });
   $('#cantidad').each(function() {
    $(this).focus(function() { $(this).select(); } );
    var elem = $(this);
   // Save current value of element
   elem.data('oldVal', elem.val());
   // Look for changes in the value
   elem.bind("propertychange onchange keyup input paste", function(event){
      // If value has changed...
      if (elem.data('oldVal') != elem.val()) {
       // Updated stored value
       elem.data('oldVal', elem.val());
       // Do action
       cant = elem.val();
       cambiarValor(value);
     }
   });
 });

  function cambiarValor(value) {
    $.ajax({
      url: "/premios/"+value+"/puntos",
    }).done(function(result) {
      console.log(parseInt(cant));
      if (parseInt(cant)) {
        if ((cant > 0)) {
          $( '#total' ).val( result * cant );
        } else {
          $( '#total' ).val( 0 );
        }
      } else {
        $( '#total' ).val( 0 );
      }

    });
  }
});

