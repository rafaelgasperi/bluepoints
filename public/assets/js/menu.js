$(function() {
    var pull        = $('#pull');
        menu        = $('.bluemenuwrapper ul');
        active      = $('.active').first().text();
    $(pull).attr('data-before',active);
    $(pull).on('click', function(e) {
        e.preventDefault();
        menu.toggle();
    });
});