/**
 * Created by AlessandroC on 9/22/14.
 */

$(document).ready(function() {
    $("input[type=submit]").click(function(){
        $("input[type=submit]").attr('disabled','disabled');
        $('form').submit();
    });
});