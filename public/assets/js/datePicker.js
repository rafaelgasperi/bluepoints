/**
 * Created by AlessandroC on 9/23/14.
 */

$('.selector-fecha').datepicker({
    format: "dd/mm/yyyy",
    endDate: "today",
    todayBtn: true,
    language: "es",
    autoclose: true,
    todayHighlight: true
});