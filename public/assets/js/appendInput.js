/**
 * Created by AlessandroC on 9/23/14.
 */

$(document).ready(function() {
    var max_fields      = 100; //maximum input boxes allowed
    var wrapper         = $(".modelos"); //Fields wrapper
    var add_button      = $(".add_model_button"); //Add button ID
    var x = 1; //initlal text box count
    var title = [];
    var options = '';
    var eliminar = '';
    $.get('/eliminar-modelo', function(data) {
        eliminar = data;
    });

    $.getJSON('/modelos', function(data){
        var count = 1;
        var count2 = 1;
        for(var i in data) {
            title[count] = i;
             if(data.hasOwnProperty(i)){
                    count++;
                }
            }
            for (var a in i){
                if(data[i].hasOwnProperty(a)){
                    count2++;
            }
        }

        for (var x = 1; x < count; x++) {
            options += "<optgroup label="+ title[x] +">";
            $.each(data[title[x]],function(index,value){
                options += '<option value="' + index + '">' + value + '</option>';
            });
        }

        append = '<div class="modelo"><div class="form-group"><div class="row"><div class="col-md-5"><label for="modelo" class="pull-right text-right">Seleccione Modelo</label></div><div class="col-md-4"><select class="form-control" name="modelo[]">'+ options +'</select></div><div class="col-md-3"><div class="remove_field"><span class="plus-button">'+ eliminar +'</span></a></div></div></div></div></div>';
    });

    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();


        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append(append+''); //add input box
        }
    });

    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).closest('.modelo').remove(); x--;
    })
});