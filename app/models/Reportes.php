<?php

class Reportes extends \Eloquent {

    protected $fillable = ['reporte'];

    protected $table = 'reportes';

    function codigos() {
        return $this->belongsTo('Bluepoints\Codigos\Codigos','codigo_id');
    }
}