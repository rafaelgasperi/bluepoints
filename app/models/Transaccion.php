<?php

class Transaccion extends \Eloquent {
	protected $fillable = [];
    protected $table = "transaccion";

    public function registro() {
        return $this->belongsToMany('\Registro');
    }

    public function premios() {
        return $this->belongsTo('\Premios', 'id_premio');
    }

    public function users() {
        return $this->belongsTo('Bluepoints\Users\User','user_id');
    }
    // public function codigos() {
    //     return $this->hasManyThrough('\Registros','\U' ,'codigo_id', 'registro_id');
    // }

}