<?php

class Premios extends \Eloquent {
	protected $fillable = [];

    public $timestamps = false;

    public function transacciones() {
        return $this->hasMany('\Transaccion');
    }

    public function paises() {
        return $this->belongsTo('Bluepoints\Paises\Paises');
    }
}