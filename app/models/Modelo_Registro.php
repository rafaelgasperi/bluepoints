<?php

use Bluepoints\Modelos\Modelos;

class Modelo_Registro extends \Eloquent {
	protected $fillable = [];
    protected $table ="modelo_registro";

    public function modelos()
    {
        return $this->belongsTo('Bluepoints\Modelos\Modelos', 'modelo_id');
    }

    public function registro()
    {
        return $this->belongsTo('Registro', 'id_registro');
    }

}