<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Registro extends \Eloquent {
    use SoftDeletingTrait;

    protected $dates = ['deleted_at'];

    protected $fillable = [];


    protected $table = "registro";

    public function codigos() {
        return $this->belongsTo('Bluepoints\Codigos\Codigos', 'codigo_id');
    }

    public function modelos() {
        return $this->belongsToMany('Bluepoints\Modelos\Modelos', 'modelo_registro','registro_id','modelo_id')->withPivot("cantidad", "ip_address")->withTimestamps();
    }

    public function users() {
        return $this->belongsTo('Bluepoints\Users\User', 'user_id');
    }

    public function transaccion() {
        return $this->belongsToMany('\Transaccion');
    }
}