@extends('templates.main-layout')

@section('content')
<div class="col-md-2"></div>
<div class="col-md-8">
    <div class="whitecontent">
        <div class="row">
            <div class="col-md-12">
                <h3>{{ trans('comoParticipo.titulo')}}</h3>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-12">
                <p>
                    {{ trans('comoParticipo.parrafo1')}}<br><br>
                    <img src="{{ URL::asset('assets/images/icons-numbered-bullets/1.png') }}">{{ trans('comoParticipo.paso1')}}<br>
                    <img src="{{ URL::asset('assets/images/icons-numbered-bullets/2.png') }}">{{ trans('comoParticipo.paso2')}}<br>
                    <img src="{{ URL::asset('assets/images/icons-numbered-bullets/3.png') }}">{{ trans('comoParticipo.paso3')}}<br>
                    <img src="{{ URL::asset('assets/images/icons-numbered-bullets/4.png') }}">{{ trans('comoParticipo.paso4')}}<br>
                    <img src="{{ URL::asset('assets/images/icons-numbered-bullets/5.png') }}">{{ trans('comoParticipo.paso5')}}<br>
                    <img src="{{ URL::asset('assets/images/icons-numbered-bullets/6.png') }}">{{ trans('comoParticipo.paso6')}}<br>
                    <img src="{{ URL::asset('assets/images/icons-numbered-bullets/7.png') }}">{{ trans('comoParticipo.paso7')}}<br>
                    <br>
                    {{ trans('comoParticipo.en_cuenta')}}<br>
                    <img src="{{ URL::asset('assets/images/icons-numbered-bullets/1.png') }}">{{ trans('comoParticipo.en_cuenta1')}}<br>
                    <img src="{{ URL::asset('assets/images/icons-numbered-bullets/2.png') }}">{{ trans('comoParticipo.en_cuenta2')}}<br>
                    <img src="{{ URL::asset('assets/images/icons-numbered-bullets/3.png') }}">{{ trans('comoParticipo.en_cuenta3')}}<br>
                    <img src="{{ URL::asset('assets/images/icons-numbered-bullets/4.png') }}">{{ trans('comoParticipo.en_cuenta4')}}<br>
                    <img src="{{ URL::asset('assets/images/icons-numbered-bullets/5.png') }}">{{ trans('comoParticipo.en_cuenta5')}}<br>
                    <img src="{{ URL::asset('assets/images/icons-numbered-bullets/6.png') }}">{{ trans('comoParticipo.leer_reglamento')}}<br>
                </p>
            </div>
        </div>
    </div>
    </div>
    <div class="col-md-2"></div>
@stop