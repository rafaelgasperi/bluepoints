<div class="col-md-12">
    <div class="stats-panel">
        <div class="row">
            <div class="col-md-4">
                <div class="stat">
                    <div class="stat-title">
                        <p>{{ trans('home.codigos_activos')}}</p>
                    </div>
                    <div class="stat-icon">
                        <img src="{{ URL::asset('assets/images/icon-codigos-activos.png') }}" alt="">
                    </div>
                    <div class="stat-points">
                        <span class="pts">
                            {{ $currentUser->registro()->count() }}
                        </span>
                    </div>
                </div>

            </div>
            <div class="col-md-4">
                <div class="stat">
                    <div class="stat-title">
                        <p>{{ trans('home.puntos_acumulados')}}</p>
                    </div>
                    <div class="stat-icon">
                        <img src="{{ URL::asset('assets/images/icon-puntos-acumulados.png') }}" alt="">
                    </div>
                    <div class="stat-points">
                        <span class="pts">
                            {{ $currentUser->registro()->sum('puntos_restantes') }}
                        </span>
                    </div>
                </div>

            </div>
            <div class="col-md-4">
                <div class="stat">
                    <div class="stat-title">
                        <p>{{ trans('home.premios_canjeados')}}</p>
                    </div>
                    <div class="stat-icon">
                        <img src="{{ URL::asset('assets/images/icon-premios-canjeados.png') }}" alt="">
                    </div>
                    <div class="stat-points">
                        <span class="pts">
                            {{ $currentUser->transaccion()->count() }}
                        </span>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>