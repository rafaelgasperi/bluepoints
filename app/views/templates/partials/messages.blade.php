@if (Session::has('messages'))

<div id="outcome" class="white-popup mfp-hide">
  <h3>{{ trans('registro.informacion')}}</h3>
  <ul>
      <li>{{ Session::get('messages') }}</li>
  </ul>
</div>

<script>
  function launchPopup() {
    $.magnificPopup.open({
      items: {
        src: '#outcome',
        type: 'inline'
      },
      fixedContentPos: 'auto',
    });
  }
  (function() {
    launchPopup();
  })();

</script>
@endif