@if ($errors->any())

<div id="outcome" class="white-popup mfp-hide">
  <h3>Error:</h3>
  <ul>
    @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>

<script>
  function launchPopup() {
    $.magnificPopup.open({
      items: {
        src: '#outcome',
        type: 'inline'
      },
      fixedContentPos: 'auto',
    });
  }
  (function() {
    launchPopup();
  })();

</script>
@endif