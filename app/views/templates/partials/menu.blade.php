<div class="nav clearfix">
    <ul class="list-inline clearfix">
        <li class="{{ setActive('home') }}"><a href="{{ URL::route('home') }}">{{ trans('menu.home')}}</a></li>
        @if(!$currentUser)
            <li class="{{ setActive('registro_path') }}"><a href="{{ URL::route('registro_path') }}">{{ trans('menu.registro')}}</a></li>
            <li class="{{ setActive('como_participar_path') }}"><a href="{{ URL::route('como_participar_path') }}">{{ trans('menu.como_participar')}}</a></li>
            <li class="{{ setActive('reglamentos_path') }}"><a href="{{ URL::route('reglamentos_path') }}">{{ trans('menu.reglas')}}</a></li>
            <li class="{{ setActive('login_path') }}"><a href="{{ URL::route('login_path') }}">{{ trans('menu.cuenta')}}</a></li>
        @else
            <li class="{{ setActive('registro_codigo_path') }}"><a href="{{ URL::route('registro_codigo_path') }}">{{ trans('menu.reg_codigo')}}</a></li>
            <li class="{{ setActive('cambiar_premio_path') }}"><a href="{{ URL::route('cambiar_premio_path') }}">{{ trans('menu.cambio')}}</a></li>
            <li class="{{ setActive('tabla_puntos_path') }}"><a href="{{ URL::route('tabla_puntos_path') }}">{{ trans('menu.tablapuntos')}}</a></li>
            <li class="{{ setActive('historial_path') }}"><a href="{{ URL::route('historial_path') }}">{{ trans('menu.historial')}}</a></li>
            <li class="{{ setActive('reglamentos_path') }}"><a href="{{ URL::route('reglamentos_path') }}">{{ trans('menu.reglas')}}</a></li>
        @endif
    </ul>
    <a href="#" id="pull"></a>
</div>