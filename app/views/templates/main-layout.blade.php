<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title>Blue Points</title>
  <link rel="stylesheet" href="{{ URL::asset('assets/vendor/bootstrap/dist/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('assets/vendor/magnific-popup/dist/magnific-popup.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/main.css') }}">
  @yield('extra-styles')

  <script src="{{ URL::asset('assets/js/respond.js') }}"></script>
  <script src="http://code.jquery.com/jquery-1.10.0.js"></script>
  <script src="{{ URL::asset('assets/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
  <script src="{{ URL::asset('assets/vendor/magnific-popup/dist/jquery.magnific-popup.min.js') }}"></script>
  <script src="{{ URL::asset('assets/js/menu.js') }}"></script>


  <!--[if gte IE 9]>
    <style type="text/css">
      .white-popup {
         filter: none;
         background: #102F93;
      }
    </style>
  <![endif]-->
  <script>
   (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
   (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
   m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
   })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

   ga('create', 'UA-46590200-2', 'auto');
   ga('send', 'pageview');

  $( document ).ready( function() {
      $('.modal-admin').css('width', '850px');
      $( '#myModal' ).modal( 'toggle' );
  });
  </script>

</head>
<body>
  <div id="bluebar"></div>
  <div class="blueheaderwrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-1 col-xs-1 c idioma esp"><a href="/es"><img class="hidden-md hidden-lg" src="{{ URL::asset('assets/images/icons-home/espanol.png') }}"></a></div>
        <div class="col-md-1 col-xs-1 idioma"><a href="/en"><img class="hidden-md hidden-lg" src="{{ URL::asset('assets/images/icons-home/ingles.png') }}"></a></div>
        <div class="col-md-8">
          <div class="blueheader">
            @if(Request::segment(1) == 'en')
              <img src="{{ URL::asset('assets/images/logo_en.png') }}">
            @else
              <img src="{{ URL::asset('assets/images/Capa-12-original.png') }}">
            @endif
          </div>
        </div>
        <div class="col-md-1 idioma hidden-xs hidden-sm">{{ trans('home.espanol')}}&nbsp;<a href="/es"><img src="{{ URL::asset('assets/images/icons-home/espanol.png') }}"></a></div>
        <div class="col-md-1 idioma hidden-xs hidden-sm">{{ trans('home.ingles')}}&nbsp;<a href="/en"><img src="{{ URL::asset('assets/images/icons-home/ingles.png') }}"></a></div>
      </div>
    </div>
  </div>

  <div class="bluemenuwrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
        @section('menu')
          @include('templates.partials.menu')
        @show
        </div>
        <div class="col-md-2"></div>
      </div>
    </div>
  </div>

  <div class="bluespacewrapper fondo">
    <div class="container-fluid">
      <div class="row">
        @yield('content')
      </div>
    </div>
  </div>

  <div class="bluefootermenu">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-2 col-xs-12">
          <img src="{{ URL::asset('assets/images/logo1.png') }}">
        </div>
        <div class="col-md-8 hidden-xs hidden-sm">
        @section('menu')
          @include('templates.partials.menu')
        @show
        </div>
        <div class="col-md-2 col-xs-12"><img src="{{ URL::asset('assets/images/logo2.png') }}"></div>
      </div>
    </div>
  </div>

    @include('templates.partials.errors')
    @include('templates.partials.messages')

    <script src="{{ URL::asset('assets/js/submitForm.js') }}"></script>
     <?php if ($_SERVER['HTTP_CF_IPCOUNTRY'] == "TT" or $_SERVER['HTTP_CF_IPCOUNTRY'] == "DO"){ ?>
    <script type="text/javascript">

      (function() {
      var host = ("https:" == document.location.protocol ? "https://secure.eemt.se" : "http://www.eemt.se");
      document.write(unescape("%3Cscript src='" + host + "/gt/js/4776.js' type='text/javascript'%3E%3C/script%3E"));
      })();

    </script>
    <?php } ?>
    @yield('scripts')
    <script type="text/javascript" src="//assets.zendesk.com/external/zenbox/v2.6/zenbox.js"></script>
    <style type="text/css" media="screen, projection">
      @import url(//assets.zendesk.com/external/zenbox/v2.6/zenbox.css);
    </style>
    <script type="text/javascript">
      if (typeof(Zenbox) !== "undefined") {
        Zenbox.init({
          dropboxID:   "20167249",
          url:         "https://clicproducction.zendesk.com",
          tabTooltip:  "Ayuda",
          tabImageURL: "https://p4.zdassets.com/external/zenbox/images/tab_es_help.png",
          tabColor:    "#1463A9",
          tabPosition: "Left"
        });
      }
    </script>
  </body>
</html>
