@extends('templates.main-layout')

@section('content')
   <div class="col-md-2"></div>
   <div class="col-md-8">
     <div class="bluecontent">
        <div class="row">
        <div class="col-md-6">
            <div class="home-action">
                <div class="icon">
                    <img src="{{ URL::asset('assets/images/icons-home/crea-tu-cuenta-icon.png') }}" alt=""/>
                </div>
                <div class="bar tu-cuenta">
                    <p class="{{ setActive('registro_path') }}"><a href="{{ URL::route('registro_path') }}">{{ trans('home.crear_cuenta')}}</a></p>
                </div>
            </div>
        </div>
            <div class="col-md-6">
                <div class="home-action">
                    <div class="icon">
                        <img src="{{ URL::asset('assets/images/icons-home/ingresa-tu-codigo-icon.png') }}" alt=""/>
                    </div>
                    <div class="bar tu-codigo">
                        <p class="{{ setActive('registro_codigo_path') }}"><a href="{{ URL::route('registro_codigo_path') }}">{{ trans('home.ingresar_codigo')}}</a></p>
                    </div>
                </div>
            </div>
            </div>
            <div class="linea-divisora">
               <img src="{{ URL::asset('assets/images/icons-home/linea-divisora.png') }}" alt=""/>
            </div>
            <div class="row">
            <div class="col-md-6">
                <div class="home-action">
                    <div class="icon">
                        <img src="{{ URL::asset('assets/images/icons-home/usa-tus-puntos-icon.png') }}" alt=""/>
                    </div>
                    <div class="bar tu-punto">
                        <p class="{{ setActive('cambiar_premio_path') }}"><a href="{{ URL::route('cambiar_premio_path') }}">{{ trans('home.usar_puntos')}}</a></p>
                    </div>
                </div>
            </div>
                <div class="col-md-6">
                    <div class="home-action">
                        <div class="icon">
                            <img src="{{ URL::asset('assets/images/icons-home/reclama-tu-premio-icon.png') }}" alt=""/>
                        </div>
                        <div class="bar tu-premio">
                            <p class="{{ setActive('historial_path') }}"><a href="{{ URL::route('historial_path') }}">{{ trans('home.reclamar_premios')}}</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="white-bar">
                        <h4>{{ trans('home.premios')}}</h4>
                    </div>
                </div>
            </div>

            <div class="row center-block">
                <div class="col-md-10 col-md-offset-1">
                    <div class="list_carousel responsive">
                        <ul id="premios1">
                            @foreach ($premios as $img)
                                <li><img src="{{URL::asset(getImgPath(NULL).$img['img_url'])}}"/><br><span class="titleSlide">{{ $img['title'] }}</span></li>
                            @endforeach
                        </ul>
                        <div class="clearfix"></div>
                        <a id="prev" class="prev" href="#"><</a>
                        <a id="next" class="next" href="#">></a>
                    </div>
                </div>
            </div>
            <p><em>{{ trans('home.reference_images') }}</em></p>
        </div>
   </div>
   </div>
   <div class="col-md-2"></div>
@stop

@section('scripts')
<script src="{{ URL::asset('assets/js/jquery.carouFredSel-6.2.1-packed.js') }}"></script>
<script src="{{ URL::asset('assets/js/slider.js') }}"></script>

@stop
