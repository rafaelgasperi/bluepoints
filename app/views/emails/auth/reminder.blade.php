<p>{{ trans('registro.email_recuperar_saludo').' '.ucwords($user->nombre.' '.$user->apellido)}}</p><br />
<p>{{ trans('registro.email_recuperar_mensaje1')}} <a href="{{ URL::action('password.reset', array($token)) }}">{{ URL::action('password.reset', array($token)) }}</a></p><br />
<p>{{ trans('registro.email_recuperar_despedida')}}</p><br />
<p>{{ trans('registro.email_mensaje5')}}</p>