<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Email</title>
</head>
<body>
    <h4>Codigo Anulado</h4>
    <p>El usuario {{ $user->nombre . ' ' . $user->apellido }} ha solicitado anular el codigo: {{ $codigo->codigo }}, id: {{ $codigo->id }} </p>
    <p>La factura se encuentra en el registro de id: {{ $factura->id }}</p>
    <br>
    <p>El comentario del usuario: {{ $reporte->reporte }} </p>
</body>
</html>