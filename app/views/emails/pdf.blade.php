<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{ trans('premios.email_titulo')}}</title>
</head>
<body>
    <h4>{{ trans('premios.email_titulo')}}!</h4>
    <p>{{ trans('premios.email_nombre')}}: {{ $user->nombre.' '.$user->apellido}}</p>
    <p>{{ trans('premios.email_telefono')}}: {{ $user->telefono}}</p>
    <p>{{ trans('premios.email')}}: {{ $user->email}}</p>
    <p>{{ trans('premios.email_pais')}}: {{ $user->pais}}</p>
    <br />
    <p>{{ trans('premios.email_content', array('premio' => $premio, 'transaccion' => $transaccion))}}</p>
</body>
</html>