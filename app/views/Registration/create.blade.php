@extends('templates.main-layout')

@section('content')

@if(isset($user))
    {{ Form::model($user, ['route' => ['edit_user'], 'method' => 'post']) }}
@else
    {{ Form::open(['route' => 'registro_path']) }}
@endif

  <div class="col-md-1 col-lg-2 col-xs-1">

  </div>
  <div class="col-xs-10 col-md-10 col-lg-8">
  <div class="bluecontent">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <div class="row">
              <div class="col-md-5">
                {{ Form::label('nombre', trans('registro.nombre').':', ['class'=>'pull-right text-right']) }}
              </div>
              <div class="col-md-7">
                  @if (isset($user))
                    {{ Form::text('nombre', null, ['class' => 'form-control','disabled' => 'disabled']) }}
                  @else
                    {{ Form::text('nombre', null, ['class' => 'form-control','placeholder' => trans('registro.nombre_placeholder')]) }}
                  @endif
              </div>
            </div>

          </div>
           <div class="form-group">
            <div class="row">
              <div class="col-md-5">
                {{ Form::label('apellido', trans('registro.apellido').':', ['class' => 'pull-right text-right']) }}
              </div>
              <div class="col-md-7">
                @if (isset($user))
                  {{ Form::text('apellido', null, ['class' => 'form-control','disabled' => 'disabled']) }}
                @else
                  {{ Form::text('apellido', null, ['class' => 'form-control', 'placeholder' => trans('registro.apellido_placeholder')]) }}
                @endif
              </div>
            </div>
          </div>
          <div class="form-group">
           <div class="row">
             <div class="col-md-5">
              {{ Form::label('identidad', trans('registro.documento').':', ['class' => 'pull-right text-right']) }}
              <small class="pull-right text-right">{{ trans('registro.documento_indicaciones') }}</small>
            </div>
              <div class="col-md-7">
                @if (isset($user))
                  {{ Form::text('identidad', null, ['class' => 'form-control','disabled' => 'disabled']) }}
                @else
                  {{ Form::text('identidad', null, ['class' => 'form-control', 'placeholder' => trans('registro.documento_placeholder')]) }}
                @endif
              </div>
            </div>
          </div>
           <div class="form-group">
            <div class="row">
              <div class="col-md-5">
                {{ Form::label('pais', trans('registro.pais').':', ['class' => 'pull-right text-right']) }}
              </div>
              <div class="col-md-7">
                @if (isset($user))
                   {{ Form::select('pais', $paises , null, ['class' => 'form-control', 'disabled' => 'disabled']) }}
                @else
                  {{ Form::select('pais', $paises , null, ['class' => 'form-control']) }}
                @endif
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-5">
                {{ Form::label('direccion', trans('registro.direccion').':', ['class' => 'pull-right text-right']) }}
              </div>
              <div class="col-md-7">
                {{ Form::textarea('direccion', null, ['class' => 'form-control', 'rows' => 2, 'placeholder' => trans('registro.direccion_placeholder')]) }}
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-6">
        <div class="form-group">
            <div class="row">
              <div class="col-md-5">
                {{ Form::label('email', trans('registro.email').':', ['class' => 'pull-right text-right']) }}
              </div>
              <div class="col-md-7">
                @if(isset($user))
                  {{ Form::email('email', null, ['class' => 'form-control', 'disabled' => 'disabled']) }}
                @else
                  {{ Form::email('email', null, ['class' => 'form-control', 'placeholder' => trans('registro.email_placeholder')]) }}
                @endif


              </div>
            </div>
          </div>
          @unless(isset($user))
           <div class="form-group">
            <div class="row">
              <div class="col-md-5">
                {{ Form::label('email_confirmation', trans('registro.email_confirmar').':', ['class' => 'pull-right text-right', ]) }}
              </div>
              <div class="col-md-7">
                {{ Form::email('email_confirmation', null, ['class' => 'form-control', 'placeholder' => trans('registro.email_placeholder')]) }}
              </div>
            </div>
          </div>
          @endunless
        <div class="form-group">
            <div class="row">
              <div class="col-md-5 ">
                {{ Form::label('password', trans('registro.clave').':', ['class' => 'pull-right text-right']) }}
                <small class="pull-right text-right">{{ trans('registro.clave_indicaciones')}}</small>
              </div>
              <div class="col-md-7">
                {{ Form::password('password', ['class' => 'form-control', 'placeholder' => trans('registro.clave_placeholder')]) }}
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-5">
                {{ Form::label('password_confirmation', trans('registro.clave_confirmar').':', ['class' => 'pull-right text-right']) }}
              </div>
              <div class="col-md-7">
                {{ Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => trans('registro.clave_placeholder')]) }}
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-5">
                {{ Form::label('telefono', trans('registro.telefono').':', ['class' => 'pull-right text-right']) }}
              </div>
              <div class="col-md-7">
                {{ Form::text('telefono', null, ['class' => 'form-control', 'placeholder' => trans('registro.telefono_placeholder')]) }}
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-5">
                {{ Form::label('sucursal_tienda', trans('registro.tienda').':', ['class' => 'pull-right text-right']) }}
              </div>
              <div class="col-md-7">
                {{ Form::text('sucursal_tienda', null, ['class' => 'form-control', 'placeholder' => trans('registro.tienda_placeholder')]) }}
              </div>
            </div>
          </div>

          @unless(isset($user))
          <div class="form-group">
            <div class="row">
              <div class="col-md-12 col-xs-12">
                {{ Form::checkbox('tos','acepto', true, ['class' => 'pull-right text-right']) }}
                {{ Form::label('tos', trans('registro.terminos').':', ['class' => 'terms pull-right text-right']) }}
              </div>
            </div>
          </div>
          @endunless
        </div>
        </div>
        <div class="linea-divisora">
           <img src="{{ URL::asset('assets/images/icons-home/linea-divisora.png') }}" alt=""/>
        </div>
        <div class="row">
            <div class="col-md-12">
            @if(isset($user))
                {{ Form::submit('ACTUALIZAR' , ['class' => 'btn pull-right'])}}
            @else
                {{ Form::submit(trans('registro.registrar') , ['class' => 'btn pull-right'])}}
            @endif


            </div>
        </div>
      </div>
  </div>
  <div class="col-md-1 col-lg-2"></div>
  {{ Form::close() }}
@stop