@extends('templates.main-layout')

@section('content')

<div class="col-md-2"></div>
<div class="col-md-8">
    <div class="whitecontent">
        <div class="row">
            <div class="col-md-12">
                <h3>{{ trans('tablapuntos.titulo') }}</h3>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-12 scroll-box">
                <p><b>{{ trans('tablapuntos.subtitulo') }}</b></p>
            <p>
                {{ trans('tablapuntos.texto_ini') }} <br>
            </p>
            <div class="row">
                    <div class="table-responsive">
                        <table class="table reglamentos">
                            <tr>
                                <td>{{ trans('tablapuntos.modelo') }}</td>
                                <td>{{ trans('tablapuntos.puntos') }}</td>
                            </tr>

                            @for( $i= 0 ; $i < count($puntos) ; $i++)
                                @if ($i == count($puntos)/2)
                                    </table>
                                    <table class="table reglamentos">
                                    <tr>
                                        <td>{{ trans('tablapuntos.modelo') }}</td>
                                        <td>{{ trans('tablapuntos.puntos') }}</td>
                                    </tr>
                                @endif
                                <tr>
                                    <td> {{ $puntos[$i]['modelo'] }} </td>
                                    <td> {{ $puntos[$i]['puntos'] }} </td>
                                </tr>
                            @endfor
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-2"></div>
@stop