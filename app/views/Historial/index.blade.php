@extends('templates.main-layout')

@section('extra-styles')
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
@stop

@section('content')
   <div class="col-xs-1 col-md-2"></div>
   <div class="col-xs-10 col-md-8">
     <div class="col-xs-12 col-md-12 col-lg-12 bluecontent bottom">
      <p class="lead">
            {{ $currentUser->nombre .' '. $currentUser->apellido }}  <small>/ {{ $currentUser->email }}</small>
      </p>
      <hr class="hr" />
      <div class="row">
          <div class="col-md-4 col-xs-12">
            <span>{{ trans('historial.detalle') }}</span>
          </div>

          @unless(empty($codigos))

            <div class="col-md-2 col-md-offset-6 col-xs-12">
              <a class="btn pull-right btnmovil3 col-md-6 open-popup-link" href="#reporte">{{ trans('historial.anular_codigo') }}</a>
            </div>
          @endunless
      </div><br />
      
      <ul class="nav nav-tabs" role="tablist">
        <li class="active"><a data-toggle="tab" data-target="#regi">{{ trans('historial.regTab') }}</a></li>
        <li><a data-toggle="tab" data-target="#trans">{{ trans('historial.transTab') }}</a></li>
      </ul>
      
      <div class="tab-content">
        <div id="regi" class="tab-pane fade in active">
          <div class="row">
              <div id="registro" class="table-responsive scroll-box">
              @unless($registros->isEmpty())
                <table class="table text-center">
                  <tr>
                    <th class="text-center">{{ trans('historial.fecha') }}</th>
                    <th class="text-center">{{ trans('historial.comentario') }}</th>
                    <th class="text-center">{{ Lang::choice('historial.codigo', 2) }}</th>
                    <th class="text-center">{{ trans('historial.factura') }}</th>
                  </tr>
                  @foreach ($registros as $registro)                
                    <tr>
                      <td>{{ $registro->created_at->format('d-m-Y')}}</td>
                      <td>{{ trans('historial.registro')}}</td>
                      <td>{{ $registro->codigos()->first()->codigo}}</td>
                      <td>{{ $registro->factura}}</td>
                    </tr>
                  @endforeach
                </table>
              @else
                <h4>{{ trans('historial.ningun') }}</h4>
              @endif
            </div>
          </div>
        </div>

        <div id="trans" class="tab-pane fade in">
          <div class="row">
              <div id="transacciones" class="table-responsive scroll-box">
              @unless($transacciones->isEmpty())
                <table class="table text-center">
                  <tr>
                    <th class="text-center">{{ trans('historial.fecha') }}</th>
                    <th class="text-center">{{ trans('historial.comentario') }}</th>
                    <th class="text-center">{{ Lang::choice('historial.codigo', 2) }}</th>
                    <th class="text-center">{{ trans('historial.factura') }}</th>
                    <th class="text-center">{{ trans('historial.premio') }}</th>
                    <th class="text-center">{{ trans('historial.detalle_tabla') }}</th>
                  </tr>
                  @foreach ($transacciones as $transaccion)
                    <tr>
                      <td>{{ $transaccion->created_at->format('d-m-Y') }}</td>
                      <td>{{ showNota($transaccion->nota) }}</td>
                      <td>{{ $transaccion->registro()->first()->codigos()->first()->codigo }}</td>
                      <td>{{ $transaccion->registro()->first()->factura }}</td>
                      @if ($lang == 'es')
                        <td>{{ $transaccion->premios()->first()->premio }}</td>
                      @else
                        <td>{{ $transaccion->premios()->first()->premios_eng }}</td>
                      @endif
                      <td><a class="btn btn-small" href="{{URL::route('premio_transaccion', $transaccion->id)}}"><i class="fa fa-search"></i></a></td>
                    </tr>
                  @endforeach
                </table>
                @else
                  <h4>{{ trans('historial.ningun') }}</h4>
                @endif
              </div>
          </div>
        </div>

      </div>

      @unless ($transacciones->isEmpty())
      <!--<div class="row">

            <?php //echo $transacciones->links(); ?>
      </div>-->
      <div class="row">
        <div class="col-md-3 col-md-offset-6">
          <a class="btn pull-right btnmovil3 col-md-6" href="{{ URL::route('historial_pdf')}}">{{ trans('historial.descargar') }}</a>
        </div>
        <div class="col-md-3">
          <a class="btn pull-right btnmovil3 col-md-6" href="{{ URL::route('historial_email')}}">{{ trans('historial.desc_email') }}</a>
        </div>
      </div>

       <div class="row">
        {{ trans('historial.intro_email') }}
        {{ trans('historial.'.$pais.'_email') }}
        {{ trans('historial.end_email') }}
      </div>

      @endunless
    </div>

    <div id="reporte" class="white-popup mfp-hide">
      <h3>{{ $currentUser->nombre .' '. $currentUser->apellido }}</h3>
      {{ Form::open(['route' => 'anular_codigo']) }}
        <div class="form-group">
          <div class="row">
            <div class="col-md-5">
              {{ Form::label('codigo', Lang::choice('historial.codigo', 1).':', ['class'=>'pull-right text-right']) }}
            </div>
            <div class="col-md-7">
              {{ Form::select('codigo', $codigos, null , ['class' => 'form-control'])}}
            </div>
          </div>
        </div>
         <div class="form-group">
          <div class="row">
            <div class="col-md-5">
              {{ Form::label('comentario', Lang::choice('historial.comentario', 1).':', ['class'=>'pull-right text-right']) }}
            </div>
            <div class="col-md-7">
              {{ Form::textarea('comentario', null, ['class' => 'form-control', 'rows' => 3, 'placeholder' => trans('historial.anular_placeholder')]) }}
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              {{ Form::submit(trans('historial.anular_button') , ['class' => 'btn pull-right btnmovil'])}}
            </div>
          </div>
        </div>
      {{ Form::close() }}
    </div>

   <div class="col-xs-1 col-md-2 col-lg-3"></div>


@include('templates.partials.errors')
@stop

@section('scripts')
  <script src="{{ URL::asset('assets/js/reportes.js') }}"></script>
  <script type="text/javascript">
    $('#transacciones a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
      })
    $('#registro a').click(function (e) {
      e.preventDefault()
      $(this).tab('show')
    })
  </script>
@stop