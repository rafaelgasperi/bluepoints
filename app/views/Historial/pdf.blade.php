<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Pdf</title>
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/main.css') }}">
  <style>
    body,html {
      background-image: none !important;
      font-family: sans-serif;
    }
    .letter {
      color: #000;
      width: 800px;
      padding-top: 50px;
    }
    .letter img {
      display: block;
      margin: 0 auto;
      margin-bottom: 50px;
    }
    .letter p {
      text-align: center;
    }
    .table-responsive {
      margin: 0 auto;
    }
    #tabla-transaccion {
      margin: 30px auto;
    }
    table {
        border-collapse: collapse;
    }
    table, th, td {
        border: 1px solid black;
        padding: 5px;
    }
    h1 {
        padding-left: 333px;
    }
  </style>
</head>
<body>
  <div class="letter">

    <img src="{{ asset('assets/images/logo1.png') }}" alt="">
    <br>
    <h1>Historial</h1>
      <p>{{ trans('historial.regTab') }}</p>
      <table class="table-responsive">
        <tr>
          <th>{{ trans('historial.fecha') }}</th>
          <th>{{ trans('historial.comentario') }}</th>
          <th>{{ Lang::choice('historial.codigo', 2) }}</th>
          <th>{{ trans('historial.factura') }}</th>
        </tr>
        @foreach ($registros as $registro)
          <tr>
            <td>{{ $registro->created_at->format('d-m-Y')}}</td>
            <td>{{ trans('historial.registro')}}</td>
            <td>{{ $registro->codigos()->first()->codigo}}</td>
            <td>{{ $registro->factura}}</td>
          </tr>
        @endforeach
      </table>
      <br>
      <p>{{ trans('historial.transTab') }}</p>

      <table class="table-responsive">
        <tr>
        <th>{{ trans('historial.fecha') }}</th>
        <th>{{ trans('historial.comentario') }}</th>
        <th>{{ Lang::choice('historial.codigo', 2) }}</th>
        <th>{{ trans('historial.factura') }}</th>
        <th>{{ trans('historial.premio') }}</th>
        </tr>
        @foreach ($transacciones as $transaccion)
          <tr>
            <td>{{ $transaccion->created_at->format('d-m-Y') }}</td>
            <td>{{ showNota($transaccion->nota) }}</td>
            <td>{{ $transaccion->registro()->first()->codigos()->first()->codigo }}</td>
            <td>{{ $transaccion->registro()->first()->factura }}</td>
            @if ($lang == 'es')
              <td>{{ $transaccion->premios()->first()->premio }}</td>
            @else
              <td>{{ $transaccion->premios()->first()->premios_eng }}</td>
            @endif
          </tr>
        @endforeach
      </table>
  </div>
</body>
</html>