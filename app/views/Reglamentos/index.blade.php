@extends('templates.main-layout')

@section('content')
<div class="col-md-2"></div>
<div class="col-md-8">
    <div class="whitecontent">
        <div class="row">
            <div class="col-md-12">
                <h3>{{ trans('reglas.titulo')}}</h3>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-12 scroll-box">
                <p>{{ trans('reglas.parrafo1')}}</p>
                <p><b>{{ trans('reglas.incentivo')}}</b></p>
                <p>{{ trans('reglas.incentivo1')}}</p>
                <p>{{ trans('reglas.incentivo2')}}</p>
                <p>{{ trans('reglas.incentivo3')}}</p>
                <p>{{ trans('reglas.incentivo4')}}</p>
                <p>{{ trans('reglas.incentivo5')}}</p>
                <p>{{ trans('reglas.incentivo6')}}</p>
                <p><b>{{ trans('reglas.mecanica')}}</b></p>
                <p>{{ trans('reglas.mecanica1')}}</p>
                <p>{{ trans('reglas.mecanica2')}}</p>
                <p>{{ trans('reglas.mecanica3')}}</p>
                <p>{{ trans('reglas.mecanica4')}}</p>
                <p>{{ trans('reglas.mecanica5')}}</p>
                <p>{{ trans('reglas.mecanica6')}}</p>
                <p>{{ trans('reglas.mecanica7')}}</p>
                <p>{{ trans('reglas.mecanica8')}}</p>
                <p>{{ trans('reglas.mecanica9')}}</p>
                <p>{{ trans('reglas.mecanica10')}}</p>

                <p>
                    <b>{{ trans('reglas.sobre')}}</b><br><br>
                    {{ trans('reglas.sobre1')}}
                </p>
                <p>{{ trans('reglas.sobre2')}}</p>
                <p>{{ trans('reglas.sobre3')}}</p>

                <p>
                    <b>{{ trans('reglas.condiciones')}}</b><br>
                    {{ trans('reglas.condiciones1')}}
                </p>
                <p>{{ trans('reglas.condiciones2')}}</p>
                <p>{{ trans('reglas.condiciones3')}}</p>
                <p>{{ trans('reglas.condiciones4')}}</p>
                <p>{{ trans('reglas.condiciones5')}}</p>
                <p>{{ trans('reglas.condiciones6')}}</p>
                <p>{{ trans('reglas.condiciones7')}}</p>
                <p>{{ trans('reglas.condiciones8')}}</p>
                <p>{{ trans('reglas.condiciones9')}}</p>
                <p>{{ trans('reglas.condiciones10')}}</p>
                <p>{{ trans('reglas.condiciones11')}}</p>

                <p>
                    <b>{{ trans('reglas.duracion')}}</b><br>
                    {{ trans('reglas.duracion1')}}
                </p>

                <p>
                    <b>{{ trans('reglas.restriccion')}}</b><br>
                    {{ trans('reglas.restriccion1')}}
                </p>
                <p>{{ trans('reglas.restriccion2')}}</p>
                <p>{{ trans('reglas.restriccion3')}}</p>

                <p>
                    <b>{{ trans('reglas.participante')}}</b><br>
                    {{ trans('reglas.participante1')}}
                </p>
                <p>{{ trans('reglas.participante2')}}</p>

                <p>
                    <b>{{ trans('reglas.descalificacion')}}</b><br>
                    {{ trans('reglas.descalificacion_titulo')}}
                </p>
                <p>{{ trans('reglas.descalificacion1')}}</p>
                <p>{{ trans('reglas.descalificacion2')}}</p>
                <p>{{ trans('reglas.descalificacion3')}}</p>
                <p>{{ trans('reglas.descalificacion4')}}</p>

                <p>
                    <b>{{ trans('reglas.disposiciones')}}</b><br>
                    {{ trans('reglas.disposiciones1')}}
                </p>
                <p>{{ trans('reglas.disposiciones2')}}</p>
                <p>{{ trans('reglas.disposiciones3')}}</p>
                <p>{{ trans('reglas.disposiciones4')}}</p>

                <p>
                    <b>{{ trans('reglas.solucion')}}</b><br>
                    {{ trans('reglas.solucion_texto')}}
                </p>

                <p>
                    <b>{{ trans('reglas.liberacion')}}</b><br>
                    {{ trans('reglas.liberacion1')}}
                </p>
                <p>{{ trans('reglas.liberacion2')}}</p>
                <p>{{ trans('reglas.liberacion3')}}</p>
                <p>{{ trans('reglas.liberacion4')}}</p>
                <p>{{ trans('reglas.liberacion5')}}</p>
                <p>{{ trans('reglas.liberacion6')}}</p>
                <p>{{ trans('reglas.liberacion7')}}</p>
                <p>{{ trans('reglas.liberacion8')}}</p>
                <p>{{ trans('reglas.liberacion9')}}</p>
                <p>{{ trans('reglas.liberacion10')}}</p>
                <br>
                <p><b>{{ trans('reglas.add1')}}</b></p>
            </div>
        </div>
    </div>
    </div>
<div class="col-md-2"></div>

@stop