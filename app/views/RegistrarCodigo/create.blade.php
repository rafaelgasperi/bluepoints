@extends('templates.main-layout')

@section('extra-styles')
    <link rel="stylesheet" href=" {{ URL::asset('assets/vendor/bootstrap-3-datepicker/css/datepicker3.css') }}">
@stop

@section('content')
{{ Form::open(['route' => 'registro_codigo_path']) }}
  <div class="col-xs-1 col-md-2 col-lg-3">
  </div>
  <div class="col-xs-10 col-md-8 col-lg-6 bluecontent">
    <div class="col-md-1"></div>
    <div class="col-md-10">
      <div class="form-group">
        <div class="row">
          <div class="col-md-5">
            {{ Form::label('codigo', trans('registrocodigo.ingresar_codigo').':', ['class'=>'pull-right text-right']) }}
          </div>
          <div class="col-md-7">
            {{ Form::text('codigo', null, ['class' => 'form-control','placeholder' => trans('registrocodigo.codigo_placeholder')]) }}
          </div>
        </div>

      </div>
      <div class="form-group">
        <div class="row">
          <div class="col-md-5">
            {{ Form::label('factura', trans('registrocodigo.ingresar_factura').':', ['class' => 'pull-right text-right']) }}
          </div>
          <div class="col-md-7">
            {{ Form::text('factura', null, ['class' => 'form-control', 'placeholder' => trans('registrocodigo.factura_placeholder')]) }}
          </div>
        </div>
      </div>
      <div class="modelos scroll-box2">
      <div class="form-group">
       <div class="row">
         <div class="col-md-5">
          {{ Form::label('modelo', trans('registrocodigo.seleccionar_modelo'), ['class' => 'pull-right text-right']) }}
        </div>
          <div class="col-md-4">
            {{ Form::select('modelo[]', $modelos, null, ['class' => 'form-control' ]) }}
          </div>
          <div class="col-md-3">
            <div class="add_model_button">
               <span class="plus-button">{{ trans('registrocodigo.add_models') }}</span>
            </div>

          <!-- <div class="col-md-2">
            <input type="text" value="1" class="form-control" name="cant[]"/>
          </div>
          <div class="col-md-1">
            <div class="add_model_button">
               <span class="plus-button">+</span>
            </div> -->
          </div>
        </div>
      </div>
      </div>
      <div class="form-group">
        <div class="row">
          <div class="col-md-5">
            {{ Form::label('almacen', trans('registrocodigo.almacen').':', ['class' => 'pull-right text-right']) }}
          </div>
          <div class="col-md-7">
            {{ Form::text('almacen', null, ['class' => 'form-control', 'placeholder' => trans('registrocodigo.sucursal_placeholder')]) }}
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="row">
          <div class="col-md-5">
            {{ Form::label('fechaVenta', trans('registrocodigo.fecha_factura').':', ['class' => 'pull-right text-right']) }}
          </div>
          <div class="col-md-7">
            {{ Form::text('fechaVenta', null, ['class' => 'form-control selector-fecha', 'placeholder' => trans('registrocodigo.fecha_placeholder')]) }}
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-1"></div>
    <div class="row">
      <div class="col-md-12">
        <hr>
      </div>
      <div class="col-md-12">
        <div class="form-group">
        {{ Form::submit(trans('registrocodigo.activar_codigo') , ['class' => 'btn pull-right btnmovil'])}}
        </div>
      </div>
    </div>

  </div>
  <div class="col-xs-1 col-md-2 col-lg-3"></div>
{{ Form::close() }}
@include('templates.partials.errors')
@stop

@section('scripts')
    <script src="{{ URL::asset('assets/vendor/bootstrap-3-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ URL::asset('assets/vendor/bootstrap-3-datepicker/js/locales/bootstrap-datepicker.es.js') }}"></script>
    <script src="{{ URL::asset('assets/js/datePicker.js') }}"></script>
    <script src="{{ URL::asset('assets/js/appendInput.js') }}"></script>
@stop