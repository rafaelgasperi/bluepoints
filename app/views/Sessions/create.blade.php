@extends('templates.main-layout')

@section('content')
    <div class="col-md-3">
    </div>
    <div class="col-md-6">
      <div class="login-box">
        {{ Form::open(['route' => 'login_path']) }}
          <div class="form-group">
            {{ Form::label('email', trans('login.email').':') }}
            {{ Form::email('email', null, ['class'=>'form-control']) }}
          </div>
          <div class="form-group">
            {{ Form::label('password', trans('login.clave').':') }}
            {{ Form::password('password', ['class' => 'form-control']) }}
          </div>
          <div class="row">
            <div class="col-md-5">
              <p class="{{ setActive('registro_path') }}" style="color:#fff;">{{ trans('login.registro')}}<a href="{{ URL::route('registro_path') }}">{{ trans('login.aqui')}}</a></p>
            </div>
            <div class="col-md-7">
              <a href="{{ URL::route('password_remind_path') }}" class="btn btn-primary pull-left btnmovil2 col-md-6 col-xs-12">{{ trans('login.recuperar_clave')}}</a>
              {{ Form::submit(trans('login.iniciar_sesion'), ['class' => 'btn btn-primary pull-right btnmovil2 col-md-6 col-xs-12'])}}
            </div>
          </div>
        {{ Form::close() }}
      </div>
    </div>
    <div class="col-md-3">
    </div>
@stop