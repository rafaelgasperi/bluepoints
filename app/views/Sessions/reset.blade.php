
@extends('templates.main-layout')

@section('content')
    <div class="col-md-3">
    </div>
    <div class="col-md-6">
      <div class="login-box">

        {{ Form::open(['route' => ['password.update', $token], 'class'=>'form-horizontal']) }}
          <div class="form-group">
          <div class="col-md-4">
            {{ Form::label('email', trans('registro.email')) }}
          </div>
          <div class="col-md-8">
            {{ Form::email('email', null, ['class'=>'form-control', 'placeholder' => trans('registro.email_placeholder')]) }}
          </div>
          </div>
          <div class="form-group">
            <div class="col-md-4">
                {{ Form::label('password', trans('registro.contrasena').':') }}
                <small style="color: #fff;">{{ trans('registro.clave_indicaciones')}}</small>
            </div>
            <div class="col-md-8">
                {{ Form::password('password', ['class'=>'form-control', 'placeholder' => trans('registro.clave_placeholder')]) }}
            </div>
          </div>
          <div class="form-group">
          <div class="col-md-4">
            {{ Form::label('password_confirmation', trans('registro.clave_confirmar').':') }}
          </div>
          <div class="col-md-8">
            {{ Form::password('password_confirmation', ['class'=>'form-control', 'placeholder' => trans('registro.clave_placeholder')]) }}
          </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              {{ Form::submit(trans('registro.cambiar_contrasena'), ['class' => 'btn btn-primary pull-right'])}}
            </div>
          </div>
            {{ Form::hidden('token', $token) }}
        {{ Form::close() }}
      </div>
    </div>
    <div class="col-md-3">
    </div>
@stop