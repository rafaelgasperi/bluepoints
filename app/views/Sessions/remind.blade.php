@extends('templates.main-layout')

@section('content')
    <div class="col-md-3">
    </div>
    <div class="col-md-6">
      <div class="login-box">

        {{ Form::open(['route' => 'password_remind_path']) }}
          <div class="form-group">
            {{ Form::label('email', trans('registro.email')) }}
            {{ Form::email('email', null, ['class'=>'form-control', 'placeholder'=>trans('registro.email_placeholder')]) }}
          </div>
          <div class="row">
            <div class="col-md-12">
              {{ Form::submit(trans('registro.recuperar_contrasena'), ['class' => 'btn btn-primary pull-right'])}}
            </div>
          </div>
        {{ Form::close() }}
      </div>
    </div>
    <div class="col-md-3">
    </div>
@stop