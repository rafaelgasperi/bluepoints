@extends('templates.main-layout')

@section('content')
   <div class="col-xs-1 col-md-2"></div>
   <div class="col-xs-10 col-md-8">
     <div class="bluecontent">
       <div class="row">
        <div class="col-md-9">
          <p class="lead">
              {{ $currentUser->nombre .' '. $currentUser->apellido }}  <small>/ {{ $currentUser->email }}</small>
          </p>
        </div>
        <div class="col-md-3">
          <p class="lead text-center">
            <a class="logout" href="{{ URL::route('logout_path') }}">{{ trans('menu.cerrar')}}</a>
          </p>
        </div>
      </div>
        <p>
          <a class="cambiar-datos" href="{{URL::route('edit_user') }}">({{ trans('home.cambiar_datos')}})</a>
        </p>
        @include('templates.partials.stats')
        <div class="row">
          <div class="col-md-12 white-bar">
            <h4>{{ trans('home.premios_disponibles')}}</h4>
          </div>
        </div>
        <div class="row center-block">
                <div class="col-md-10 col-md-offset-1">
                    <div class="list_carousel responsive">
                        <ul id="premios1">
                            @foreach ($img as $ima)
                                <li><img src="{{URL::asset(getImgPath($currentUser->pais).$ima->img_url)}}" /><br><span class="titleSlide"> {{ $ima->disponible}} {{ trans('home.unidades')}}</span></li>
                            @endforeach
                        </ul>
                        <div class="clearfix"></div>
                        <a id="prev" class="prev" href="#"><</a>
                        <a id="next" class="next" href="#">></a>
                    </div>
                </div>
            </div>
            <p><em>{{ trans('home.reference_images') }}</em></p>
     </div>
   </div>
   <div class="col-xs-1 col-md-2"></div>
@stop

@section('scripts')
<script src="{{ URL::asset('assets/vendor/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/jquery.carouFredSel-6.2.1-packed.js') }}"></script>
<script src="{{ URL::asset('assets/js/slider.js') }}"></script>

@stop