@extends('templates.main-layout')

@section('content')
	<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="bluecontent">
				<div class="row">
					<div class="col-md-8 col-sm-8 col-xs-12">
						<div class="premiosTitle" type="text">PREMIOS</div>
					</div>
					<div class="col-md-1 col-sm-4 col-xs-12 premiosTop">
						{{ Form::label('pais', 'Pais:', ['class' => 'pull-right text-right']) }}
					</div>
					<div class="col-md-3">
						{{ Form::select('pais', $paises , null, ['class' => 'form-control premios pais']) }}
					</div>
				</div>
				<div class="container-premios-pais">
					@include('Premios.container')
				</div>
			</div>
		</div>
	<div class="col-md-2"></div>
@stop

@section('scripts')
<script src="{{ URL::asset('assets/vendor/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/jquery.carouFredSel-6.2.1-packed.js') }}"></script>
<script src="{{ URL::asset('assets/js/sliderPremios.js') }}"></script>
<script src="{{ URL::asset('assets/js/sliderajax.js') }}"></script>

@stop