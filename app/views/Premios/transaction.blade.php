@extends('templates.main-layout')

@section('content')
    <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="deepbluewrapper">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div class="deepbluecontent">
                       <div class="form-group">
                            <div class="row">
                                <div class="col-md-5">
                                    {{ Form::label('codigo',trans('transaction.transaccion'), ['class'=>'pull-right text-right'])}}
                                </div>
                                <div class="col-md-5">
                                    {{ Form::input('text', 'codigo', $transaccion->id, ['class' => 'form-control', 'disabled' => 'disabled'])}}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-5">
                                    {{ Form::label('puntos',trans('transaction.puntos'), ['class'=>'pull-right text-right'])}}
                                </div>
                                <div class="col-md-5">
                                    {{ Form::input('text', 'puntos', $transaccion->puntos_usados, ['class' => 'form-control', 'disabled' => 'disabled'])}}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-5">
                                    {{ Form::label('premios',trans('transaction.premios'), ['class'=>'pull-right text-right']) }}
                                </div>
                                <div class="col-md-5">
                                    {{ Form::input('text', 'premios',$transaccion->cantidad. ' ' . mostrarPremio($transaccion), ['class' => 'form-control', 'disabled' => 'disabled'])}}
                                </div>
                            </div>
                        </div>
                        <p class='text-center'>{{trans('transaction.presentar')}}</p>
                        <div class="table-responsive">
                          <table class="table text-center">
                            <tr>
                              <th class="text-center">{{trans('transaction.codigo')}}</th>
                              <th class="text-center">{{trans('transaction.factura')}}</th>
                              <th class="text-center">{{trans('transaction.modelo')}}</th>
                              <th class="text-center">{{trans('transaction.cantidad')}}</th>
                            </tr>
                            @foreach ($transaccion->registro()->get() as $registro)
                                @foreach ($registro->modelos()->get() as $modelos)
                                <tr>
                                    <td>{{ $registro->codigos()->first()->codigo }}</td>
                                    <td>{{ $registro->factura }}</td>
                                    <td>{{ $modelos->modelo }}</td>
                                    <td>{{ $modelos->pivot->cantidad }}</td>
                                </tr>
                                @endforeach
                            @endforeach
                          </table>
                        </div>
                      </div>
                    </div>
                <div class="col-md-1"></div>
                <div class="text-center">
                    <a id="descargapdf" href="{{ URL::route('transaccion_pdf', $transaccion->id ) }}" class="btn">{{trans('transaction.pdf')}}</a>
                </div>
            </div>

        </div>
    <div class="col-md-2"></div>
@stop