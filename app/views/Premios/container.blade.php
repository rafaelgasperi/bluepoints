<div class="row center-block">
    <div class="col-md-10 col-md-offset-1">
        <div class="row">
            <div class="list_carousel responsive">
                <ul id="premios3">
                    @foreach ($img as $ima)
                        <li><img src="{{URL::asset($ima)}}" /></li>
                    @endforeach
                </ul>
                <a id="prev2" class="prev2" href="#">z<</a>
                <a id="next2" class="next2" href="#">></a>
            </div>
        </div>
    </div>
</div>