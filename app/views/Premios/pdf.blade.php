<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Pdf</title>
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/main.css') }}">
  <style>
    body,html {
      background-image: none !important;
      font-family: sans-serif;
    }
    .letter {
      color: #000;
      width: 800px;
      padding-top: 50px;
    }
    .letter img {
      display: block;
      margin: 0 auto;
      margin-bottom: 50px;
    }
    .letter p {
      text-align: center;
    }
    .letter p:last-child {
      width: 80%;
      display: block;
      margin: 0 auto;
    }
    .table-responsive {
      margin: 0 auto;
    }
    #tabla-transaccion {
      margin: 30px auto;
    }
    table {
        border-collapse: collapse;
    }

    table, th, td {
        border: 1px solid black;
    }
  </style>
</head>
<body>
  <div class="letter">

    <p class='text-center'>{{trans('transaction.presentar')}}</p>
    <p>{{trans('transaction.transaccion')}}: {{$transaccion->id}} </p>
    <p>{{trans('transaction.puntos')}}: {{ $transaccion->puntos_usados }}</p>
    <p>{{trans('transaction.premios')}}: {{ $transaccion->cantidad. ' ' . mostrarPremio($transaccion) }}</p>
    <div class="table-responsive">
      <table id="tabla-transaccion" class="table text-center">
        <tr>
          <th class="text-center">{{trans('transaction.codigo')}}</th>
          <th class="text-center">{{trans('transaction.factura')}}</th>
          <th class="text-center">{{trans('transaction.modelo')}}</th>
          <th class="text-center">{{trans('transaction.cantidad')}}</th>
        </tr>
        @foreach ($transaccion->registro()->get() as $registro)
            @foreach ($registro->modelos()->get() as $modelos)
                <tr>
                  <td>{{ $registro->codigos()->first()->codigo }}</td>
                  <td>{{ $registro->factura }}</td>
                  <td>{{ $modelos->modelo }}</td>
                  <td>{{ $modelos->pivot->cantidad }}</td>
                </tr>
            @endforeach
        @endforeach
      </table>
    </div>
     <p style="width: 80%;">
        {{ trans('historial.intro_email') }}
      </p>
        <p>
        {{ trans('historial.'.$currentUser->pais.'_email') }}
        </p>
      <p>
        {{ trans('historial.end_email') }}
      </p>
  </div>

</body>
</html>