@extends('templates.main-layout')

@section('content')
   <div class="col-xs-1 col-md-2"></div>
   <div class="col-xs-10 col-md-8">
     <div class="col-xs-12 col-md-12 col-lg-12 bluecontent bottom">
      <p class="lead">
            {{ $currentUser->nombre .' '. $currentUser->apellido }}  <small>/ {{ $currentUser->email }}</small>
      </p>
      <hr class="hr" />
      <div class="row">
        <div class="col-md-3">
            <span>{{ trans('premios.puntos_disponibles')}}</span>
            <p class="premiosTitle">{{ $currentUser->registro()->sum('puntos_restantes') }}</p>
        </div>
        <div class="col-md-9">
            {{ Form::open(['route' => 'cambiar_premio_path']) }}
              <div class="form-group">
                <div class="row">
                  <div class="col-md-5">
                    {{ Form::label('premio', trans('premios.premios_disponibles').':', ['class'=>'pull-right text-right']) }}
                  </div>
                  <div class="col-md-7">
                    {{ Form::select('premio', $premios, null, ['class' => 'form-control']) }}
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-md-5">
                    {{ Form::label('cantidad', trans('premios.cantidad').':', ['class'=>'pull-right text-right']) }}
                  </div>
                  <div class="col-md-7">
                    {{ Form::text('cantidad', '1', ['class' => 'form-control', 'id'=>'cantidad']) }}
                  </div>
                </div>
              </div>

              <div class="form-group">
                <div class="row">
                  <div class="col-md-5">
                    {{ Form::label('total', trans('premios.puntos_requeridos').':', ['class'=>'pull-right text-right']) }}
                  </div>
                  <div class="col-md-2">
                    {{ Form::text('total', '0' , ['class' => 'form-control input-disabled']) }}
                  </div>
                  <div class="col-md-1">{{ trans('premios.puntos')}}</div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-5 col-md-offset-7">
                  <div class="form-group">
                  {{ Form::submit(trans('premios.submit') , ['class' => 'btn pull-right'])}}
                  </div>
                </div>
              </div>
            {{ Form::close() }}
        </div>
       </div>
    </div>
   <div class="col-xs-1 col-md-2 col-lg-3"></div>

@include('templates.partials.errors')
@stop

@section('scripts')
  <script src="{{ URL::asset('assets/js/redeem.js') }}"></script>
@stop