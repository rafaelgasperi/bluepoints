<?php namespace Bluepoints\Codigos;

use Eloquent;

class Codigos extends Eloquent {
    protected $table = 'codigos';

    public function registro() {
        return $this->hasMany('\Registro');
    }

}
