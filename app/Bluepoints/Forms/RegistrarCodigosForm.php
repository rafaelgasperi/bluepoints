<?php namespace Bluepoints\Forms;

use Laracasts\Validation\FormValidator;

class RegistrarCodigosForm extends FormValidator
{
    /**
     * Validation rules for the Registration Form
     * @var [type]
     */

    protected $rules = [
        'codigo' => 'required|exists:codigos,codigo,activo,0',
        'factura' => 'required|unique_for_user',
        'modelo' => 'required|array|required_en_array',
        'almacen' => 'required',
        'fechaVenta' => 'required|date_format:d/m/Y|before:tomorrow',
    ];

    protected $messages = [
        'nombre.required' => 'El campo <strong>Nombre</strong> es Requerido',
        'apellido.required' => 'El campo <strong>Apellido</strong> es Requerido',
        'identidad.required' => 'El campo <strong>Documento de Identidad</strong> es Requerido',
        'identidad.unique' => 'La Identidad sumsinsitrada ya se encuentra registrada en el Sistema',
        'email.required' => 'El campo <strong>E-mail</strong> es Requerido',
        'email.unique' => 'El Correo Electronico sumsinsitrado ya se encuentra registrado en el Sistema',
        'email.confirmed' => 'El Correo Electronico sumsinsitrado no es igual al de confirmación',
        'password.required' => 'El campo <strong>Contraseña</strong> es Requerido',
        'password.confirmed' => 'La Contraseña sumsinsitrada no es igual a la de confirmación',
        'telefono.required' => 'El campo <strong>Teléfono</strong> es Requerido',
        'pais.required' => 'El campo <strong>Pais</strong> es Requerido',
        'sucursal_tienda.required' => 'El campo <strong>sucursal_tienda</strong> es Requerido',
        'direccion.required' => 'El campo <strong>Dirección</strong> es Requerido',
        'tos.required' => 'Debe Aceptar los <strong>Terminos y Condiciones</strong>'
  ];
}
