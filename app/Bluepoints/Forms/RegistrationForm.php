<?php namespace Bluepoints\Forms;

use Laracasts\Validation\FormValidator;

class RegistrationForm extends FormValidator
{
  /**
   * Validation rules for the Registration Form
   * @var [type]
   */
  protected $rules = [
    'nombre' => 'required',
    'apellido' => 'required',
    'identidad' => 'required|unique:users',
    'email' => 'required|unique:users|email|confirmed',
    'password' => 'required|confirmed',
    'telefono' => 'required',
    'pais' => 'required',
    'sucursal_tienda' => 'required',
    'direccion' => 'required',
    'tos' => 'required'
  ];
}