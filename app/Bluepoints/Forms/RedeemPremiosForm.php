<?php namespace Bluepoints\Forms;

use Laracasts\Validation\FormValidator;

class RedeemPremiosForm extends FormValidator
{
    /**
     * Validation rules for the Registration Form
     * @var [type]
     */
    protected $rules = [
        'premio' => 'required|exists:premios,id',
        'cantidad' => 'required|positive|cantidad_disponible|min:1|numeric',
        'total' => 'required|puntos_disponibles',
    ];

    protected $messages = [
        'cantidad.required' => 'El campo <strong>Cantidad</strong> es Requerido',
        'cantidad.positive' => 'El campo <strong>Cantidad</strong> Tiene que ser un numero valido',
       // 'premio.required' => 'El campo <strong>Premio</strong> es Requerido',
        'premio.exists' => 'No se encuentra el premio seleccionado',
      //  'total.required' => 'El Campo <strong>Total</strong> es Requerido',
        'total.puntos_disponibles' => 'No posee los puntos suficientes para el cambio',
    ];
}
