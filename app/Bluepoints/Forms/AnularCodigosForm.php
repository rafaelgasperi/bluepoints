<?php namespace Bluepoints\Forms;

use Laracasts\Validation\FormValidator;

class AnularCodigosForm extends FormValidator {
    protected $rules = [
        'codigo' => 'required',
        'comentario' => 'required',
    ];
}