<?php namespace Bluepoints\Forms;

use Laracasts\Validation\FormValidator;

class SignInForm extends FormValidator
{
    /**
     * Validation rules for the Registration Form
     * @var [type]
     */
    protected $rules = [
        'email' => 'required',
        'password' => 'required',
    ];
    protected $messages = [
        'email.required' => 'El campo <strong>E-mail</strong> es Requerido',
        'password.required' => 'El campo <strong>Contraseña</strong> es Requerido',
    ];
}