<?php namespace Bluepoints\Forms;

use Laracasts\Validation\FormValidator;

class ChangePasswordForm extends FormValidator {
    protected $rules = [
        'email' => 'required',
        'password' => 'required|confirmed',
    ];
} 