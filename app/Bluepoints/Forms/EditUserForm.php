<?php namespace Bluepoints\Forms;

use Laracasts\Validation\FormValidator;

class EditUserForm extends FormValidator
{
    /**
     * Validation rules for the Registration Form
     * @var [type]
     */
    protected $rules = [
    'password' => 'required|confirmed',
    'telefono' => 'required',
    'sucursal_tienda' => 'required',
    'direccion' => 'required',
  ];

    protected $messages = [
        'cant.required' => 'El campo <strong>Cantidad</strong> es Requerido',
        'cant.numeric_en_array' => 'El campo <strong>Cantidad</strong> Tiene que ser un numero valido',
        'codigo.required' => 'El campo <strong>Código</strong> es Requerido',
        'codigo.exists' => 'El código no existe o ya se encuentra registrado',
        'factura.required' => 'El Campo <strong>Factura</strong> es Requerido',
        'factura.unique' => 'Esta factura ya esta ingresada en el Sistema',
        'modelo.required' => 'El campo <strong>Modelo</strong> es Requerido',
        'modelo.diferente_en_array' => 'Los modelos a agregar tienen que ser diferentes',
        'almacen.required' => 'El campo <strong>Almacén</strong> es Requerido',
        'fechaVenta.required' => 'El campo <strong>Fecha</strong> es Requerido',
        'fechaVenta.date_format' => 'La fecha tiene que estar en el formato DD/MM/AAAA (ej: 23/06/2014)',
        'fechaVenta.before' => 'La fecha de la factura no puede ser posterior a la fecha actual',
    ];
}