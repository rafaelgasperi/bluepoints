<?php namespace Bluepoints\Mailers;

use Illuminate\Mail\Mailer as Mail;
abstract class Mailer {

  private $mail;

  function __construct(Mail $mail) {
    $this->mail = $mail;
  }
  /**
   * Enviar Correo
   * @param  [type] $user    [description]
   * @param  [type] $subject [description]
   * @param  [type] $view    [description]
   * @param  [type] $data    [description]
   * @return [type]          [description]
   */
  public function sendTo($user, $subject, $view, $view_data = [] ) {
    $this->mail->queue($view, $view_data, function($message) use($user, $subject)
    {
      $message->to($user->email)->subject($subject);
    });
  }
}