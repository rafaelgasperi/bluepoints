<?php namespace Bluepoints\Mailers;

use Bluepoints\Users\User;
use Illuminate\Support\Facades\URL;
class UserMailer extends Mailer {

  public function sendRegisterConfirmationMessageTo(User $user) {
    $subject = trans('registro.email_subject');
    $view = 'emails.Registration.confirm';
     $user_data = [
         'userConfirmation' => URL::route('verificar_registro_path', $user->confirmationId),
         'nombre'           => $user->nombre.' '.$user->apellido
    ];

    return $this->sendTo($user, $subject, $view, $user_data);
  }
}