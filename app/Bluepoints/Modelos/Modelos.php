<?php namespace Bluepoints\Modelos;

class Modelos extends \Eloquent {
	protected $fillable = ['id_registro', 'modelo_id', 'cantidad'];

    public function registro()
    {
        return $this->belongsToMany('\Registros', 'registro', 'modelo_id', 'id_registro');
    }
}