<?php namespace Bluepoints\Handlers;

use Laracasts\Commander\Events\EventListener;
use Bluepoints\Registration\Events\UserRegistered;
use Bluepoints\Mailers\UserMailer;

class EmailNotifier extends EventListener {
  private $mailer;
  public function __construct(UserMailer $mailer) {
    $this->mailer = $mailer;
  }
  public function whenUserRegistered(UserRegistered $event) {
    $this->mailer->sendRegisterConfirmationMessageTo($event->user);
  }
}