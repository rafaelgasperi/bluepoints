<?php namespace Bluepoints\Registration;

use Laracasts\Commander\CommandHandler;
use Laracasts\Commander\Events\DispatchableTrait;
use Bluepoints\Users\UserRepository;
use Bluepoints\Users\User;


class RegisterUserCommandHandler implements CommandHandler
{
  use DispatchableTrait;
  protected $repository;

  function __construct(UserRepository $repository) {
    $this->repository = $repository;
  }

  public function handle($command)
  {
    $user = User::register(
        $command->nombre,
        $command->apellido,
        $command->identidad,
        $command->email,
        $command->password,
        $command->telefono,
        $command->pais,
        $command->direccion,
        $command->confirmationId,
        $command->sucursal_tienda
      );
    $this->repository->save($user);
    $this->dispatchEventsFor($user);
    return $user;
  }
}