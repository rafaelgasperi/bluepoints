<?php namespace Bluepoints\Registration\Events;

use Bluepoints\Users\User;

class UserRegistered {
  public $user;

  function __construct(User $user)
  {
    $this->user = $user;
  }
}