<?php namespace Bluepoints\Registration;

class RegisterUserCommand
{
  public $nombre;
  public $apellido;
  public $identidad;
  public $email;
  public $password;
  public $telefono;
  public $pais;
  public $sucursal_tienda;
  public $direccion;
  public $confirmationId;

  function __construct($nombre, $apellido, $identidad, $email, $password, $telefono, $pais, $sucursal_tienda, $direccion,$confirmationId) {
    $this->nombre = $nombre;
    $this->apellido = $apellido;
    $this->identidad = $identidad;
    $this->email = $email;
    $this->password = $password;
    $this->telefono = $telefono;
    $this->pais = $pais;
    $this->sucursal_tienda = $sucursal_tienda;
    $this->direccion = $direccion;
    $this->confirmationId = $confirmationId;
  }

}