<?php namespace Bluepoints\CodigoRegistrado;

use Eloquent;
use Bluepoints\Codigos\Codigos;

class CodigoRegistrado extends Eloquent {
    protected $table = 'codigo_registrado';

    protected $fillable = ['codigo','factura','modelo','almacen','fecha'];

    public function codigos() {
        return $this->belongsTo('Bluepoints\Codigos\Codigos');
    }
   public static function activar($codigo, $factura, $modelo, $almacen, $fecha) {
        $codigoObject = Codigos::where('codigo','=',$codigo)->first();
        $codigoObject->activo = 1;
        $codigoObject->save();
        $codigoRegistrado = new static(compact('codigos_id','factura','modelo','almacen','fechaVenta'));
        $codigoRegistrado->codigos()->associate($codigoObject);
        $codigoRegistrado->save();
        return $codigoRegistrado;
   }
}
