<?php namespace Bluepoints\Paises;

class Paises extends \Eloquent {

	protected $table = "paises";

	protected $fillable = ['id', 'nompais', 'abvpais'];

    public function premios() {
        return $this->belongsToMany('\Premios', 'premios_pais','pais_av','premio_id');
    }
}