<?php namespace Bluepoints\Users;

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Eloquent, Hash;
use Laracasts\Commander\Events\EventGenerator;
Use Bluepoints\Registration\Events\UserRegistered;
Use Bluepoints\Codigos\Codigos;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait, EventGenerator;

	/**
	 * Que campos pueden ser mass-assigned?
	 * @var [type]
	 */
	protected $fillable = ['nombre', 'apellido', 'identidad', 'email', 'telefono', 'pais', 'direccion', 'password','confirmationId', 'sucursal_tienda'];

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	/**
	 * Always Store Hashed passwords
	 * @param [type] $password [description]
	 */
	public function setPasswordAttribute($password)
	{
		$this->attributes['password'] = Hash::make($password);
	}

	public function registro() {
		return $this->hasMany('\Registro');
	}
	public function transaccion() {
		return $this->hasMany('\Transaccion','user_id');
	}

	public function codigos() {
		return $this->hasMany('\Registro');
	}

	/**
	 * Register a new User
	 * @param  [type] $nombre    [description]
	 * @param  [type] $apellido  [description]
	 * @param  [type] $identidad [description]
	 * @param  [type] $email     [description]
	 * @param  [type] $password  [description]
	 * @param  [type] $telefono  [description]
	 * @param  [type] $pais      [description]
	 * @param  [type] $direccion [description]
	 * @param  [type] $sucursal_tienda [description]
	 * @return [type]            [description]
	 */
	public static function register($nombre, $apellido, $identidad, $email, $password, $telefono, $pais, $direccion, $confirmationId, $sucursal_tienda)
	{
			$user = new static(compact('nombre', 'apellido', 'identidad', 'email', 'password', 'telefono', 'pais', 'direccion', 'confirmationId', 'sucursal_tienda'));
			$user->raise(new UserRegistered($user));
			return $user;
	}
}
