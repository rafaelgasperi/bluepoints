<?php
    use Bluepoints\Modelos\Modelos;

    function setActive($route, $class = 'active')
    {
        return (Route::currentRouteName() == $route) ? $class : '';
    }

    /**
     * Calcula los puntos que se van a agregar en puntos_restantes la primera vez que se agregan a la bd
     * @param  [type] $input [description]
     * @return [type]        [description]
     */
    function calcularPuntos($input, $pais){
        switch($pais){
            case 'ECU':
                $puntos = puntosDeModelos($input['modelo'], 'ecuador');
                break;
            case 'RD':
                $puntos = puntosDeModelos($input['modelo'], 'dominicana');
                break;
            case 'T&T':
                $puntos = puntosDeModelos($input['modelo'], 'trinidad');
                break;
            case 'CR':
                $puntos = puntosDeModelos($input['modelo'], 'costarica');
                break;
            case 'NI':
                $puntos = puntosDeModelos($input['modelo'], 'nicaragua');
                break;
            case 'JAM':
                $puntos = puntosDeModelos($input['modelo'], 'jamaica');
                break;
            case 'SV':
                $puntos = puntosDeModelos($input['modelo'], 'salvador');
                break;
            case 'HON':
                $puntos = puntosDeModelos($input['modelo'], 'honduras');
                break;
            default:
                $puntos = puntosDeModelos($input['modelo'], 'puntos');
                break;
        }
        return $puntos;
    };


    /**
     * Calcula los puntos requeridos para realizar un cambio de premio
     * @param  [type] $item [description]
     * @param  [type] $cant [description]
     * @return [type]       [description]
     */
    function calcularPuntosPremios($item, $cant){
        $pts = Premios::findOrFail($item)->puntos;
        $pts *= $cant;
        return $pts;
    };


    function getImgPath($pais)
    {
        switch ($pais) {
            case 'PA':
                    $path = 'assets/images/premios/PA/';
                break;

            case 'ECU':
                    $path = 'assets/images/premios/ECU/';
                break;

            case 'NI':
                    $path = 'assets/images/premios/NI/';
                break;

            case 'HON':
                    $path = 'assets/images/premios/HON/';
                break;

            case 'CR':
                    $path = 'assets/images/premios/CR/';
                break;

            case 'RD':
                    $path = 'assets/images/premios/RD/';
                break;

            case 'SV':
                    $path = 'assets/images/premios/SV/';
                break;

            case 'JAM':
                    $path = 'assets/images/premios/JAM/';
                break;

            case 'T&T':
                    $path = 'assets/images/premios/T&T/';
                break;

            default:
                    $path = 'assets/images/premios/GEN/';
                break;
        }

        return $path;
    }

    function showNota($nota) {
        switch ($nota) {
            case '1':
                echo trans('historial.espera');
                break;
            case '2':
                echo trans('historial.entregado');
                break;
            default:
                echo trans('historial.rechazado');
        }
    }

    function mostrarPremio($transaccion) {
        switch(Config::get('app.locale')) {
            case 'en':
                return $transaccion->premios()->first()->premios_eng;
            default:
                return $transaccion->premios()->first()->premio;
                break;
        }
    }

    function puntosParaPais($modelos, $pais) {
        $puntos = [];
        foreach($modelos as $modelo){
            $puntos[] = [
                "modelo" => $modelo['modelo'],
                "puntos" => $modelo[$pais]
            ];
        }
        return $puntos;
    }

    function puntosDeModelos($modelos, $pais) {
        $cantidad = 1;
        $puntos = 0;
        $i = 0;
        foreach ($modelos as $modelo) {
            $pts = Modelos::findOrFail($modelo)->{$pais};
            $puntos += ($pts * $cantidad );
            $i++;
        }
        return $puntos;
    }
