<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddPaisIdStringToPremiosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('premios', function(Blueprint $table)
		{
		    DB::statement('ALTER TABLE premios MODIFY COLUMN pais_id VARCHAR(255)');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('premios', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE premios MODIFY COLUMN pais_id INT');
		});
	}

}
