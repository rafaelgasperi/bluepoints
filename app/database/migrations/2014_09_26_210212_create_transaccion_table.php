<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTransaccionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transaccion', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('puntos_usados');
			$table->string('nota')->default('sin comentarios');
			$table->integer('id_premio');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transaccion');
	}

}
