<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCodigoRegistradoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('codigo_registrado', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('codigo');
			$table->string('factura');
			$table->integer('modelo');
			$table->integer('almacen');
			$table->date('fechaVenta');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('codigo_registrado');
	}

}
