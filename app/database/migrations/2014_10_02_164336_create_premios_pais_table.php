<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePremiosPaisTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('premios_pais', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('pais_avb');
			$table->integer('premio_id')->unsigned()->index();
			$table->foreign('premio_id')->references('id')->on('premios')->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('premios_pais');
	}

}
