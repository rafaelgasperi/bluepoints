<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateModeloRegistroTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//Schema::drop('modelo_registro');
		Schema::create('modelo_registro', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('modelo_id')->unsigned()->index();
			$table->foreign('modelo_id')->references('id')->on('modelos')->onDelete('cascade');
			$table->integer('registro_id')->unsigned()->index();
			$table->foreign('registro_id')->references('id')->on('registro')->onDelete('cascade');
			$table->integer('cantidad');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('modelo_registro');
		Schema::create('modelo_registro', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('modelo_id')->unsigned()->index();
			$table->foreign('modelo_id')->references('id')->on('modelos')->onDelete('cascade');
			$table->integer('registro_id')->unsigned()->index();
			$table->foreign('registro_id')->references('id')->on('registro')->onDelete('cascade');
			$table->integer('cantidad');
			$table->timestamps();
		});
	}

}
