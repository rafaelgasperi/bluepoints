<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddPremiosEngToPremiosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('premios', function(Blueprint $table)
		{
			$table->string('premios_eng');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('premios', function(Blueprint $table)
		{
			$table->dropColumn('premios_eng');
		});
	}

}
