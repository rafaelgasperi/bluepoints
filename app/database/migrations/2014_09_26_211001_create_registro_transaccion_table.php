<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRegistroTransaccionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('registro_transaccion', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('registro_id')->unsigned()->index();
			$table->foreign('registro_id')->references('id')->on('registro')->onDelete('cascade');
			$table->integer('transaccion_id')->unsigned()->index();
			$table->foreign('transaccion_id')->references('id')->on('transaccion')->onDelete('cascade');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('registro_transaccion');
	}

}
