<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkToReportesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('reportes', function(Blueprint $table)
		{
			$table->foreign('codigo_id')->references('id')->on('codigos')->onDelete('cascade');
			$table->foreign('estado_id')->references('id')->on('estados')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('reportes', function(Blueprint $table)
		{
			$table->dropForeign('reportes_codigo_id_foreign');
			$table->dropForeign('reportes_estado_id_foreign');
		});
	}

}
