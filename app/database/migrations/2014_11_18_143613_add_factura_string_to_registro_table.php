<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFacturaStringToRegistroTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('registro', function(Blueprint $table)
		{
		    DB::statement('ALTER TABLE registro MODIFY COLUMN factura VARCHAR(255)');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('registro', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE registro MODIFY COLUMN factura INT');
		});
	}

}
