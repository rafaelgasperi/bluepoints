<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFieldsToModelosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('modelos', function(Blueprint $table)
		{
			$table->string('costarica');
			$table->string('honduras');
			$table->string('nicaragua');
			$table->string('salvador');
			$table->string('jamaica');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('modelos', function(Blueprint $table)
		{
			$table->dropColumn('costarica');
			$table->dropColumn('honduras');
			$table->dropColumn('nicaragua');
			$table->dropColumn('salvador');
			$table->dropColumn('jamaica');
		});
	}

}
