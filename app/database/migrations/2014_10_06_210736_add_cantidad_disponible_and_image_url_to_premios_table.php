<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddCantidadDisponibleAndImageUrlToPremiosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('premios', function(Blueprint $table)
		{
			$table->integer('disponible');
			$table->string('img_url');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('premios', function(Blueprint $table)
		{
			$table->dropColumn('disponible');
			$table->dropColumn('img_url');
		});
	}

}
