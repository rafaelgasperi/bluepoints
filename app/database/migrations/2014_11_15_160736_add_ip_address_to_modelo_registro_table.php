<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddIpAddressToModeloRegistroTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('modelo_registro', function(Blueprint $table)
		{
			$table->string('ip_address', 45);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('modelo_registro', function(Blueprint $table)
		{
			$table->dropColumn('ip_address');
		});
	}

}
