<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddPaisIdToPremiosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('premios', function(Blueprint $table)
		{

			$table->integer('pais_id')->unsigned()->index();
			// $table->foreign('pais_id')->references('id')->on('paises')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('premios', function(Blueprint $table)
		{
 			$table->drop_foreign('pais_id');
 			// $table->dropColumn('pais_id');
		});
	}

}
