<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddOtrosPaisesToModelosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('modelos', function(Blueprint $table)
		{
			$table->string('ecuador');
			$table->string('dominicana');
			$table->string('trinidad');

		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('modelos', function(Blueprint $table)
		{
			$table->dropColumn('ecuador');
			$table->dropColumn('dominicana');
			$table->dropColumn('trinidad');
		});
	}

}
