<?php
 
class PremiosPais extends DatabaseSeeder {

	public function run(){ 
		DB::table('premios_pais')->delete();

		DB::table('premios_pais')->insert(array(				
				array( 
		        	'premio_id' => 1,
		        	'pais_avb' => 'PA'
		        ),
		        array( 
		        	'premio_id' => 2,
		        	'pais_avb' => 'PA'
		        ),
		        array( 
		        	'premio_id' => 3,
		        	'pais_avb' => 'PA'
		        ),
		        array( 
		        	'premio_id' => 4,
		        	'pais_avb' => 'PA'
		        ),
		        array( 
		        	'premio_id' => 5,
		        	'pais_avb' => 'PA'
		        ),
		        array( 
		        	'premio_id' => 6,
		        	'pais_avb' => 'PA'
		        ),
		        array( 
		        	'premio_id' => 7,
		        	'pais_avb' => 'PA'
		        ),
		        array( 
		        	'premio_id' => 8,
		        	'pais_avb' => 'PA'
		        ),
		        array( 
		        	'premio_id' => 9,
		        	'pais_avb' => 'PA'
		        ),
		        array( 
		        	'premio_id' => 10,
		        	'pais_avb' => 'PA'
		        ),
		        array( 
		        	'premio_id' => 11,
		        	'pais_avb' => 'PA'
		        ),
		        array( 
		        	'premio_id' => 12,
		        	'pais_avb' => 'CR'
		        ),
		        array( 
		        	'premio_id' => 13,
		        	'pais_avb' => 'CR'
		        ),
		        array( 
		        	'premio_id' => 14,
		        	'pais_avb' => 'CR'
		        ),
		        array( 
		        	'premio_id' => 15,
		        	'pais_avb' => 'CR'
		        ),
		        array( 
		        	'premio_id' => 16,
		        	'pais_avb' => 'CR'
		        ),
		        array( 
		        	'premio_id' => 17,
		        	'pais_avb' => 'CR'
		        ),
		        array( 
		        	'premio_id' => 18,
		        	'pais_avb' => 'CR'
		        ),
		        array( 
		        	'premio_id' => 19,
		        	'pais_avb' => 'CR'
		        ),
		        array( 
		        	'premio_id' => 20,
		        	'pais_avb' => 'CR'
		        ),
		        array( 
		        	'premio_id' => 21,
		        	'pais_avb' => 'CR'
		        ),
		        array( 
		        	'premio_id' => 22,
		        	'pais_avb' => 'RD'
		        ),
		        array( 
		        	'premio_id' => 23,
		        	'pais_avb' => 'RD'
		        ),
		        array( 
		        	'premio_id' => 24,
		        	'pais_avb' => 'RD'
		        ),
		        array( 
		        	'premio_id' => 25,
		        	'pais_avb' => 'RD'
		        ),
		        array( 
		        	'premio_id' => 26,
		        	'pais_avb' => 'RD'
		        ),
		        array( 
		        	'premio_id' => 27,
		        	'pais_avb' => 'RD'
		        ),
		        array( 
		        	'premio_id' => 28,
		        	'pais_avb' => 'RD'
		        ),
		        array( 
		        	'premio_id' => 29,
		        	'pais_avb' => 'RD'
		        ),
		        array( 
		        	'premio_id' => 30,
		        	'pais_avb' => 'HON'
		        ),
		        array( 
		        	'premio_id' => 31,
		        	'pais_avb' => 'HON'
		        ),
		        array( 
		        	'premio_id' => 32,
		        	'pais_avb' => 'HON'
		        ),
		        array( 
		        	'premio_id' => 33,
		        	'pais_avb' => 'HON'
		        ),
		        array( 
		        	'premio_id' => 34,
		        	'pais_avb' => 'HON'
		        ),
		        array( 
		        	'premio_id' => 35,
		        	'pais_avb' => 'HON'
		        ),
		        array( 
		        	'premio_id' => 36,
		        	'pais_avb' => 'HON'
		        ),
		        array( 
		        	'premio_id' => 37,
		        	'pais_avb' => 'HON'
		        ),
		        array( 
		        	'premio_id' => 38,
		        	'pais_avb' => 'CR'
		        ),
		        array( 
		        	'premio_id' => 39,
		        	'pais_avb' => 'SV'
		        ),
		        array( 
		        	'premio_id' => 40,
		        	'pais_avb' => 'SV'
		        ),
		        array( 
		        	'premio_id' => 41,
		        	'pais_avb' => 'SV'
		        ),
		        array( 
		        	'premio_id' => 42,
		        	'pais_avb' => 'SV'
		        ),
		        array( 
		        	'premio_id' => 43,
		        	'pais_avb' => 'SV'
		        ),
		        array( 
		        	'premio_id' => 44,
		        	'pais_avb' => 'SV'
		        ),
		        array( 
		        	'premio_id' => 45,
		        	'pais_avb' => 'SV'
		        ),
		        array( 
		        	'premio_id' => 46,
		        	'pais_avb' => 'T&T'
		        ),
		        array( 
		        	'premio_id' => 47,
		        	'pais_avb' => 'T&T'
		        ),
		        array( 
		        	'premio_id' => 48,
		        	'pais_avb' => 'T&T'
		        ),
		        array( 
		        	'premio_id' => 49,
		        	'pais_avb' => 'T&T'
		        ),
		        array( 
		        	'premio_id' => 50,
		        	'pais_avb' => 'T&T'
		        ),
		        array( 
		        	'premio_id' => 51,
		        	'pais_avb' => 'T&T'
		        ),
		        array( 
		        	'premio_id' => 52,
		        	'pais_avb' => 'T&T'
		        ),
		        array( 
		        	'premio_id' => 53,
		        	'pais_avb' => 'NI'
		        ),
		        array( 
		        	'premio_id' => 54,
		        	'pais_avb' => 'NI'
		        ),
		        array( 
		        	'premio_id' => 55,
		        	'pais_avb' => 'NI'
		        ),
		        array( 
		        	'premio_id' => 56,
		        	'pais_avb' => 'NI'
		        ),
		        array( 
		        	'premio_id' => 57,
		        	'pais_avb' => 'NI'
		        ),
		        array( 
		        	'premio_id' => 58,
		        	'pais_avb' => 'NI'
		        ),
		        array( 
		        	'premio_id' => 59,
		        	'pais_avb' => 'NI'
		        ),
		        array( 
		        	'premio_id' => 60,
		        	'pais_avb' => 'NI'
		        ),
		        array( 
		        	'premio_id' => 61,
		        	'pais_avb' => 'CR'
		        ),
		        array( 
		        	'premio_id' => 62,
		        	'pais_avb' => 'JAM'
		        ),
		        array( 
		        	'premio_id' => 63,
		        	'pais_avb' => 'JAM'
		        ),
		        array( 
		        	'premio_id' => 64,
		        	'pais_avb' => 'JAM'
		        ),
		        array( 
		        	'premio_id' => 65,
		        	'pais_avb' => 'JAM'
		        ),
		        array( 
		        	'premio_id' => 66,
		        	'pais_avb' => 'JAM'
		        ),
		        array( 
		        	'premio_id' => 67,
		        	'pais_avb' => 'JAM'
		        ),
		        array( 
		        	'premio_id' => 68,
		        	'pais_avb' => 'JAM'
		        ),
		        array( 
		        	'premio_id' => 69,
		        	'pais_avb' => 'JAM'
		        ),
		        array( 
		        	'premio_id' => 70,
		        	'pais_avb' => 'ECU'
		        ),
		        array( 
		        	'premio_id' => 71,
		        	'pais_avb' => 'ECU'
		        ),
		        array( 
		        	'premio_id' => 72,
		        	'pais_avb' => 'ECU'
		        ),
		        array( 
		        	'premio_id' => 73,
		        	'pais_avb' => 'ECU'
		        ),
		        array( 
		        	'premio_id' => 74,
		        	'pais_avb' => 'ECU'
		        ),
		        array( 
		        	'premio_id' => 75,
		        	'pais_avb' => 'ECU'
		        ),
		        array( 
		        	'premio_id' => 76,
		        	'pais_avb' => 'ECU'
		        )
	        )
		);
	}
}