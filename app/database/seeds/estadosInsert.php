<?php

class estadosInsert extends DatabaseSeeder {

	public function run(){ 
		DB::table('estados')->insert(array(
			array(
		        	'esp' => 'En espera',
		        	'eng' => 'Waiting'					 
		        ),

			array(
					'esp' => 'Anulado',
					'eng' => 'Canceled'
				),

			array(
					'esp' => 'Rechazado',
					'eng' => 'Rejected'
				)
			)
		);
	}
}