<?php
 
class Paises extends DatabaseSeeder {

	public function run(){
		DB::table('paises')->insert(array(				
					array( 
			        	'nompais' => 'Panamá',
			        	'abvpais' => 'PA'
			        ),

			        array( 
			        	'nompais' => 'Ecuador',
			        	'abvpais' => 'ECU'
			        ),

			        array( 
			        	'nompais' => 'Nicaragua',
			        	'abvpais' => 'NI'
			        ),

			        array( 
			        	'nompais' => 'Honduras',
			        	'abvpais' => 'HON'
			        ),

			        array( 
			        	'nompais' => 'Costa Rica',
			        	'abvpais' => 'CR'
			        ),

			        array( 
			        	'nompais' => 'República Dominicana',
			        	'abvpais' => 'RD'
			        ),

			        array( 
			        	'nompais' => 'El Salvador',
			        	'abvpais' => 'SV'
			        ),

			        array( 
			        	'nompais' => 'Jamaica',
			        	'abvpais' => 'JAM'
			        ),

			        array( 
			        	'nompais' => 'Trinidad y Tobago',
			        	'abvpais' => 'T&T'
			        ),
			));			
	}
}

?>