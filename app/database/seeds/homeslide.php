<?php

class homeslide extends DatabaseSeeder {

	public function run(){ 
		DB::table('slider_home')->insert(array(
			array(
		        	'title' => 'Bono de Supermercado',
		        	'img_url' => 'megamaxi-bonus.jpg',
		        	'title_eng' => 'Supermarket Bonus'
		        ),

			array(
		        	'title' => 'Smartphone',
		        	'img_url' => 'Galaxy-S5-Mini.jpg',
		        	'title_eng' => 'Smartphone'
		        ),

			array(
		        	'title' => 'Galaxy Tab',
		        	'img_url' => 'galaxy-tab-10.jpg',
		        	'title_eng' => 'Galaxy Tab'
		        ),

			array(
		        	'title' => 'TV LED',
		        	'img_url' => 'tv-LED-32.jpg',
		        	'title_eng' => 'TV LED'
		        ),

			array(
		        	'title' => 'Estadía en Hotel',
		        	'img_url' => 'estadia-hotel.jpg',
		        	'title_eng' => 'Hotel Stay'
		        ),

			array(
		        	'title' => 'Lavadora',
		        	'img_url' => 'lavadora.jpg',
		        	'title_eng' => 'Washing Machine'
		        ),

			array(
		        	'title' => 'Microondas',
		        	'img_url' => 'microondas.jpg',
		        	'title_eng' => 'Microwave'
		        )
			)
		);
	}
}