<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		//$this->call('PremiosInsert');
		//$this->call('PremiosPais');
		//$this->call('homeslide');
		//$this->call('estadosInsert');
		//$this->call('modelosInsert');
		$this->call('cmsInsert');

	}

}
