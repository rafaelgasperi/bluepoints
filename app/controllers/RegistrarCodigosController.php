<?php

use Bluepoints\Forms\RegistrarCodigosForm;
use Bluepoints\Almacenes\Almacenes;
use Bluepoints\Modelos\Modelos;
use Bluepoints\Codigos\Codigos;
use Bluepoints\CodigoRegistrado\CodigoRegistrado;

class RegistrarCodigosController extends \BaseController {


	private $registrarCodigosForm;
	public function __construct(RegistrarCodigosForm $registrarCodigosForm) {
        $this->registrarCodigosForm = $registrarCodigosForm;
        $this->beforeFilter('auth');
    }

	/**
	 * Display a listing of the resource.
	 * GET /registrarcodigos
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /registrarcodigos/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$modelos = [];
		foreach (Modelos::all() as $modelo) {
			$modelos[$modelo->grupo][$modelo->id] = $modelo->modelo;
        }
        $modelos = array_merge(['' => 'Select Model'], $modelos);
		return View::make('RegistrarCodigo.create', compact('modelos'));
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /registrarcodigos
	 *
	 * @return Response
	 */
	public function store()
	{
        $this->registrarCodigosForm->validate(Input::all());

        extract(Input::only('factura','almacen','codigo','modelo','cant','fechaVenta'));

        $fechaVenta = str_replace('/', '-', $fechaVenta);
        $pais = Auth::user()->pais;

        $puntosRestantes = (int) calcularPuntos(Input::only('modelo','cant'), $pais);
        // esto esta chancloño, pero funciona. refactor later..
        $registro = new Registro;
        $code = Codigos::whereCodigo(Input::only('codigo'))->first();
        $code->activo = 1;
        $code->save();
        $codigo = Codigos::find((int)$code->id);
        $registro->factura = $factura;
        $registro->nombre_tienda = $almacen;
        $registro->puntos_restantes = $puntosRestantes;
        $registro->puntos_iniciales = $puntosRestantes;

        $registro->fecha =  date('Y-m-d', strtotime($fechaVenta));
        $registro->users()->associate(Auth::user());
        Auth::user()->pais_ip = $_SERVER['HTTP_CF_IPCOUNTRY']; //$_SERVER["REMOTE_ADDR"];
        Auth::user()->save();
        $registro->codigos()->associate($codigo);
        $registro->ip_address = Request::getClientIp();
 		// fin de la creacion de registro

        DB::transaction(function() use ($registro, $modelo, $cant)
        {
            $registro->save();
            $i = 0;
            foreach ($modelo as $m) {
                $model = Modelos::findOrFail($m);
                $registro->modelos()->attach($model->id, ['cantidad' => 1, 'ip_address' => Request::getClientIp()]); //hardcoding.
                $i++;
            }
        });

 		// otro codigo chancloño, just for the deliver... refactor later.

        $messages = trans('registrocodigo.exito', array('puntos' => $puntosRestantes));
		return Redirect::home()->with(compact('messages'));
	}

	/**
	 * Display the specified resource.
	 * GET /registrarcodigos/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /registrarcodigos/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /registrarcodigos/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /registrarcodigos/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

    public function modelos() {
        $modelos = [];
        foreach (Modelos::all() as $modelo) {
            $modelos[$modelo->grupo][$modelo->id] = $modelo->modelo;
        }
        //var_dump($modelos);
        return Response::json($modelos);
    }
}
