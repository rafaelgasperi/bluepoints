<?php

/*
	|--------------------------------------------------------------------------
	| Controlador para el manejo de premios
	|--------------------------------------------------------------------------
	|
	| 	Registro, canje y listado de premios
	|
	|	Route::get('/', 'PremiosController@showPremios');
	|
	|	@author: Arturo Rossodivita
	|
	*/

use Bluepoints\Users\user;
use Bluepoints\Forms\RedeemPremiosForm;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Bluepoints\Paises\Paises;



class PremiosController extends BaseController {

	function __construct(RedeemPremiosForm $redeemPremiosForm)
	{
		$this->redeemPremiosForm = $redeemPremiosForm;
        $this->beforeFilter('auth', ['except' => ['showPremios', 'showPremiosPais']]);
    }
	public function showPremios()
	{
			$paises = Paises::select('abvpais','nompais')->get()->toArray();

			foreach ($paises as $pais) {
				$paisesSelect[$pais['abvpais']] = $pais['nompais'];
				$paisabv[$pais['abvpais']] = $pais['abvpais'];
			}

			$paisRan = $paisabv[array_rand($paisabv)];
			$paisRan = print_r($paisRan, true);

			$pathImg = 'assets/images/premios/'.$paisRan;
			$files = File::allFiles($pathImg);

			foreach ($files as $file){
			    $img[] = (string)$file;
			}

			$imgRev = array_reverse($img);

			return View::make('Premios.index', array('paises' => $paisesSelect, 'img' => $img, 'imgRev' => $imgRev));
	}

	public function showPremiosPais($id) {
			$paisesList = Paises::select('abvpais','nompais')->get()->toArray();

			foreach ($paisesList as $pais) {
				$paisesSelect[$pais['abvpais']] = $pais['nompais'];
				$paisabv[$pais['abvpais']] = $pais['abvpais'];
			}

			$pathImg = 'assets/images/premios/'.$id;
			$files = File::allFiles($pathImg);

			foreach ($files as $file){
			    $img[] = (string)$file;
			}

			$imgRev = array_reverse($img);

			return View::make('Premios.index', array('paises' => $paisesSelect, 'img' => $img, 'imgRev' => $imgRev));
	}

	public function redeem()
	{
		$prizes = [];

		$pais = Auth::user()->pais;
		$lang = Config::get('app.locale');

		if($lang=='es'){
			$prizes = Premios::select('id','premio')->where('pais_id',$pais)->where('disponible','>','0')->get();
			foreach ($prizes as $premio) {
                $premios[$premio->id] = $premio->premio;
			}
		}else{
			$prizes = Premios::select('id','premios_eng')->where('pais_id',$pais)->where('disponible','>','0')->get();
			foreach ($prizes as $premio) {
                $premios[$premio->id] = $premio->premios_eng;
			}
		}

		return View::make('Premios.redeem', compact('premios'));
	}

	public function transaction($transaccion_id)
	{
		try {
			$id = Auth::user()->id;
			$transaccion = Transaccion::find($transaccion_id);
			return View::make('Premios.transaction', compact('transaccion'));
		} catch(ModelNotFoundException $e) {
			return Redirect::route('home');
		}
	}

	public function store()
	{
		$this->redeemPremiosForm->validate(Input::all());
		extract(Input::only('premio', 'cantidad'));

		// obtenemos los puntos necesarios
		$premio = Premios::find($premio);
		$puntos_necesarios = $premio->puntos * $cantidad;

		// buscamos los registros del usuario
		$registros = Registro::whereUserId(Auth::user()->id)->where('puntos_restantes','>','0')->get();

		// se crea una transaccion
		$transaccion = new Transaccion;
		$transaccion->puntos_usados = $puntos_necesarios;
		$transaccion->nota = "1";
		$transaccion->cantidad = $cantidad;
		$transaccion->premios()->associate($premio);
		$transaccion->users()->associate(Auth::user());
        $transaccion->ip_address = Request::getClientIp();


		DB::transaction(function() use ($registros, $puntos_necesarios, $transaccion, $premio, $cantidad)
		{
			$premio->decrement('disponible', $cantidad);
			$premio->save();
			$transaccion->save();
		    foreach ($registros as $registro) {
	    		if ($puntos_necesarios != 0)  {
		    		if ($registro['puntos_restantes'] <= $puntos_necesarios) {
				    	$puntos_necesarios -= $registro['puntos_restantes'];
		    			$transaccion->registro()->attach($registro);
		    			$registro->puntos_restantes = 0;
		    			$registro->save();
			    	} else if ($registro['puntos_restantes'] > $puntos_necesarios) {
			    		$transaccion->registro()->attach($registro);
			    		$registro->decrement('puntos_restantes', $puntos_necesarios);
			    		$registro->save();
			    		$puntos_necesarios = 0;
			    	}
		    	}
		    }
		});
		PremiosController::makePdf($transaccion->id);

		return Redirect::route('premio_transaccion', ['transaccion_id' => $transaccion->id])->with('messages',trans('premios.exito'));
	}

	public static function makePdf($transaccion_id) {
		$user =  Auth::user();
		$transaccion = Transaccion::find($transaccion_id);

		define('TRANSACCIONES_DIR', public_path('uploads/Transacciones')); // I define this in a constants.php file

		  if (!is_dir(TRANSACCIONES_DIR)){
		      mkdir(TRANSACCIONES_DIR, 0755, true);
		  }

		 switch ($user->pais) {
		 	case 'PA':
		 			$email = array("charlenecruz@clic.com.pa","l.saavedra@samsung.com", "a.armijo@samsung.com","soporte@clic.com.pa");
		 		break;
		 	case 'RD':
		 			$email = array("charlenecruz@clic.com.pa","l.saavedra@samsung.com", "a.armijo@samsung.com","soporte@clic.com.pa","sheila.bido@productiva.com.do","matos.s@samsung.com", "acosta.d@samsung.com");
		 		break;
		 	case 'HON':
		 			$email = array("charlenecruz@clic.com.pa","l.saavedra@samsung.com", "a.armijo@samsung.com","soporte@clic.com.pa","chernandez@gcchn.net","rodriguez.a@samsung.com");
		 		break;
		 	case 'SV':
		 			$email = array("charlenecruz@clic.com.pa","l.saavedra@samsung.com", "a.armijo@samsung.com","soporte@clic.com.pa","bluepoints@gcccontacto.com","portillo.b@samsung.com");
		 		break;
		 	case 'T&T':
		 			$email = array("charlenecruz@clic.com.pa","l.saavedra@samsung.com", "a.armijo@samsung.com","soporte@clic.com.pa","nicholas@mangomediacaribbean.com","g.whyte@samsung.com ");
		 		break;
		 	case 'NI':
		 			$email = array("charlenecruz@clic.com.pa","l.saavedra@samsung.com", "a.armijo@samsung.com","soporte@clic.com.pa","ganadorespromobluepoints@gmail.com","r.hanon@samsung.com");
		 		break;
		 	case 'JAM':
		 			$email = array("charlenecruz@clic.com.pa","l.saavedra@samsung.com", "a.armijo@samsung.com","soporte@clic.com.pa","marketplus@cwjamaica.com","k.gardener@samsung.com");
		 		break;
		 	case 'ECU':
		 			$email = array("charlenecruz@clic.com.pa","l.saavedra@samsung.com", "a.armijo@samsung.com","soporte@clic.com.pa","gmunoz@misiva.com","castro.p@samsung.com", "r.pasquel@samsung.com");
		 		break;
	 		case 'CR':
		 			$email = array("charlenecruz@clic.com.pa","l.saavedra@samsung.com", "a.armijo@samsung.com","soporte@clic.com.pa","d.pacheco@samsung.com"," vendedores@liftbrand.com");
		 		break;

		 }
		 $premio = Premios::find($transaccion->id_premio);

		if(Request::segment(1) == 'en')
			$premio = $premio->premios_eng;
		else
			$premio = $premio->premio;

  		$view = 'Premios.pdf';
  		$view = View::make($view, compact('transaccion'));
	  	$outputName = str_random(10); // str_random is a [Laravel helper](http://laravel.com/docs/helpers#strings)
	  	$pdfPath = TRANSACCIONES_DIR.'/'.$outputName.'.pdf';
	  	File::put($pdfPath, PDF::load($view,'letter', 'portrait')->output());
	  	$data = ['user' => $user, 'transaccion' => $transaccion->id, 'premio' => $premio];
	  	Mail::send('emails.pdf', $data, function($message) use ($pdfPath, $user, $email) {
		    $message->from('bluepoints@bluepointsamsung.com', 'Bluepoints');
		    $message->to($email);
		    $message->subject(trans('premios.email_subject'));
		    $message->attach($pdfPath);
	  	});
	}

	public function pdfBlade($transaccion_id) {
		$user =  Auth::user()->id;
		try {
			$transaccion = Transaccion::findOrFail($transaccion_id);
			$view = 'Premios.pdf';
			$view = View::make($view)->with(compact('transaccion'));
			$title  = str_random(10);
			$title .= '_Blue-points';
			return PDF::load($view, 'letter', 'portrait')->download($title);
		} catch(ModelNotFoundException $e) {
			return Redirect::route('home');
		}
	}

	public function puntosDePremio($id)
	{
		return Premios::find($id)->puntos;
	}
}