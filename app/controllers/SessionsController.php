<?php

use Bluepoints\Forms\SignInForm;

class SessionsController extends \BaseController {

    /**
     * @var SignInForm
     */
    private $signInForm;

    public function __construct(SignInForm $signInForm) {

        $this->signInForm = $signInForm;
        $this->beforeFilter('guest', ['except' => 'destroy']);
    }


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('Sessions.create');
	}

	public function store()
	{
        $this->signInForm->validate(Input::all());
        $data = [
          'email' => Input::get('email'),
          'password' => Input::get('password'),
          'userConfirmed' => 1,
        ];

        if (Auth::attempt($data)) {
            return Redirect::route('home')->with('message','Bienvenido');
        } else {
            return Redirect::route('login_path')->withErrors(['El Email o Contraseña son incorrectos, o El correo no esta verificado'])->withInput();
        }
	}
  /**
   * Genera vista para generar contraseña nueva.
   * @return [type] [description]
   */
  public function remind()
  {
      return View::make('Sessions.remind');
  }

	public function destroy()
	{
		Auth::logout();
        return Redirect::home();
	}


}
