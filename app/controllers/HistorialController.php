<?php

/*
	|--------------------------------------------------------------------------
	| Controlador para el manejos del historial
	|--------------------------------------------------------------------------
	|
	| Listado del historial de transacciones
	|
	|	Route::get('/', 'HistorialController@showHistorial');
	|
	|	@author: Arturo Rossodivita
	|
	*/

use Bluepoints\Users\user;
use Bluepoints\Registros\TransaccionRegistros;
use Bluepoints\Registros\Registros;
use Bluepoints\Codigos\Codigos;
use Bluepoints\Forms\AnularCodigosForm;

class HistorialController extends BaseController {
	protected $anularCodigosForm;

	public function __construct(AnularCodigosForm $anularCodigosForm ) {
		$this->anularCodigosForm = $anularCodigosForm;
        $this->beforeFilter('auth');
	}

	public function showHistorial()
	{
		$user = Auth::user();
		$lang = Config::get('app.locale');

	  	$codigos = [];
	  	foreach ($user->registro as $registro) {

	  		if (!Reportes::Where('codigo_id', '=', $registro->id)->first()) {
	    		$codigos[$registro->id] = \Bluepoints\Codigos\Codigos::find($registro->codigo_id)->codigo;
	  		}
	  	}
		if (Auth::guest()){
			return View::make('hello');
		} else {
			$user =  Auth::user()->id;
			$pais = Auth::user()->pais;
			$transacciones = Transaccion::where('user_id', '=', $user)->get();
			$registro = Registro::where('user_id','=', $user)->get();
			return View::make('Historial.index', array('transacciones' => $transacciones, 'pais' => $pais, 'codigos' => $codigos, 'registros' => $registro, 'lang' => $lang));
		}
	}

	public function getPdfHistorial()
	{
		if (Auth::guest()){
			return View::make('hello');
		} else {
			$user =  Auth::user()->id;
			$lang = Config::get('app.locale');
			try {
				$transacciones = Transaccion::where('user_id', '=', $user)->get();
				$registro = Registro::where('user_id','=', $user)->get();
				$view = 'Historial.pdf';
				$view = View::make($view, array('transacciones'=>$transacciones, 'registros'=>$registro, 'lang'=>$lang));
				$title  = str_random(10);
				$title .= '_Blue-points';
				return PDF::load($view, 'letter', 'portrait')->download($title);
			} catch(ModelNotFoundException $e) {
				return Redirect::route('home');
			}
		}
	}
	public function getEmailHistorial()
	{

		if (Auth::guest()){
			return View::make('hello');
		} else {
			$user =  Auth::user();
			$registros = Registro::where('user_id','=', $user->id)->get();
			$transacciones = Transaccion::where('user_id', '=', $user->id)->get();
			$lang = Config::get('app.locale');


			define('HISTORIAL_DIR', public_path('uploads/Historial')); // I define this in a constants.php file

			  if (!is_dir(HISTORIAL_DIR)){
			      mkdir(HISTORIAL_DIR, 0755, true);
			  }

	  		$view = 'Historial.pdf';
	  		$view = View::make($view, compact('transacciones','registros','lang'));
		  	return $view;
		  	$outputName = str_random(10); // str_random is a [Laravel helper](http://laravel.com/docs/helpers#strings)
		  	$pdfPath = HISTORIAL_DIR.'/'.$outputName.'.pdf';
		  	File::put($pdfPath, PDF::load($view,'letter', 'portrait')->output());
		  	$data = ['user' => $user];
		  	Mail::send('emails.historial', $data, function($message) use ($pdfPath, $user){
			    $message->from('bluepoints@bluepointsamsung.com', 'Bluepoints');
			    $message->to($user->email);
			    $message->subject('Bluepoints Historial');
			    $message->attach($pdfPath);
			});
	  		return Redirect::route('historial_path')->with('messages',trans('historial.correo_msg'));
		}
	}
	public function anular() {
			$comentario = "";
			$this->anularCodigosForm->validate(Input::all());
			extract(Input::only('codigo', 'comentario'));

			$datCode = Registro::where('id','=',$codigo)
		                    ->where('user_id',Auth::user()->id)
		                    ->first();

		if($datCode)
		{
		    $codigoAnulado = Codigos::find(($datCode->codigo_id));
		    $reporte = new Reportes;
		    $reporte->reporte = $comentario;
		    $reporte->estado_id = 1;
		    $reporte->codigos()->associate(($codigoAnulado));
		    $reporte->save();
		    $data = [
		        'user' => Auth::user(),
		        'codigo' => $codigoAnulado,
		        'reporte' => $reporte,
		        'factura' => $datCode
		    ];
		    $transaccion = $datCode->transaccion()->delete();

		    Mail::send('emails.anulado', $data, function($message) use ($datCode){
		            $message->from('bluepoints@bluepointsamsung', 'Bluepoints');
		            $message->to(array('soporte@clic.com.pa', 'charlenecruz@clic.com.pa'));
		            $message->subject('Codigo Anulado '.$datCode->codigo_id);
		        });

		    $datCode->delete();

		    return Redirect::route('historial_path')->with('messages',trans('historial.anular_msg'));
		}
		else
		{
		    return Redirect::route('historial_path')->with('messages',trans('historial.anular_msg2'));
		}


	}
}




