<?php

use Bluepoints\Forms\RegistrationForm;
use Bluepoints\Registration\RegisterUserCommand;
use Bluepoints\Users\User;
use Laracasts\Commander\CommandBus;
use Bluepoints\Paises\Paises;


class RegistrationController extends \BaseController {
	/**
	 * @var CommandBus
	 */
	private $commandBus;

	/**
	 * @var RegistrationForm
	 */

	private $registrationForm;

	function __construct(\Laracasts\Commander\CommandBus $commandBus, RegistrationForm $registrationForm)
	{
		$this->registrationForm = $registrationForm;
		$this->commandBus = $commandBus;
        $this->beforeFilter('guest', ['except' => ['verificar']]);
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$paises = Paises::select('abvpais','nompais')->get();

		$paisesSelect[0] = trans('registro.pais_default');

		foreach ($paises as $pais) {
			$paisesSelect[$pais->abvpais] = $pais->nompais;
		}

		return View::make('Registration.create')->withPaises($paisesSelect);
	}

	/**
	 * Create a new user
	 * @return string
	 */

	public function store()
	{
		$this->registrationForm->validate(Input::all());
		extract(Input::only('nombre','apellido','identidad','email','password','telefono','pais','sucursal_tienda','direccion'));
        for( $code_length = 60, $newcode = ''; strlen($newcode) < $code_length; $newcode .= chr(!rand(0, 2) ? rand(48, 57) : (!rand(0, 1) ? rand(65, 90) : rand(97, 122))));

		$user = $this->commandBus->execute(new RegisterUserCommand($nombre, $apellido, $identidad, $email, $password, $telefono, $pais,$sucursal_tienda, $direccion, $newcode));
		return Redirect::home()->with('messages', trans('registro.mensaje'));
	}

    public function verificar($confirmationCode) {
        $user = User::where('confirmationId' , '=' , $confirmationCode)
                        ->where('userConfirmed', '=','0');
        if($user->count()) {
            $user = $user->first();
            $user->userConfirmed = 1;
            $user->confirmationId = 'done';
            $user->save();
            Auth::login($user);
            $message = ['title' => trans('registro.verificacion_titulo'),'messages' => trans('registro.verificacion_mensaje')];
            return Redirect::route('home')->with('title' , trans('registro.verificacion_titulo'),'messages' , trans('registro.verificacion_mensaje'));
        }
        return Redirect::route('home')->withErrors(trans('registro.verificacion_error'));
    }




}
