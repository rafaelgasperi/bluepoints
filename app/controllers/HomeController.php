<?php

use Bluepoints\Users\User;
use Bluepoints\Modelos\Modelos;
use Bluepoints\Paises\Paises;

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		if (Auth::guest()) {
            $lang = Config::get('app.locale');
            if($lang == 'es'){
			    $premios = Slider_Home::select('title','img_url')->get();
            }else if($lang == 'en'){
                $premios = Slider_Home::select('title_eng as title','img_url')->get();
            }
			return View::make('hello')->withPremios($premios);

		} else {
			$pais =  Auth::user()->pais;
            $prizes = Premios::select('disponible','img_url')->where('pais_id',$pais)->where('disponible','>','0')->get();
			// foreach ($prizes as $id) {
   //              if ($premio!='[]'){
   //                  $premios[] = $premio;
   //              }
			// }
            $premios = $prizes;
			return View::make('Sessions.index')->withImg($premios);
		}
	}

    public function showComoParticipar()
    {
        return View::make('ComoParticipo.index');
    }

    public function showReglamentos()
    {
        return View::make('Reglamentos.index');
    }

    public function showTablaPuntos()
    {
        $pais =  Auth::user()->pais;
        $modelos = Modelos::all();
        $cantidad = $modelos->count();
        switch($pais){
            case 'CR':
                $puntos = puntosParaPais($modelos, 'costarica');
                break;
            case 'HON':
                $puntos = puntosParaPais($modelos, 'honduras');
                break;
            case 'NI':
                $puntos = puntosParaPais($modelos, 'nicaragua');
                break;
            case 'SV':
                $puntos = puntosParaPais($modelos, 'salvador');
                    break;
            case 'JAM':
                $puntos = puntosParaPais($modelos, 'jamaica');
                break;
            case 'ECU':
                $puntos = puntosParaPais($modelos, 'ecuador');
                break;
            case 'RD':
                $puntos = puntosParaPais($modelos, 'dominicana');
                break;
            case 'T&T':
                $puntos = puntosParaPais($modelos, 'trinidad');
                break;
            default:
                $puntos = puntosParaPais($modelos, 'puntos');
                break;
            }

            return View::make('Puntos.index', array('puntos' => $puntos));
    }

    // public function erase()
    // {
    //     if (Auth::user(1)) {
    //         $rows = DB::table('codigos')->where('activo', '<>', 0)->update(array('activo' => 0));
    //         $rows = DB::table('codigo_registrado')->delete();
    //         $rows = DB::table('registro')->delete();
    //         $rows = DB::table('transaccion')->delete();
    //         echo 'Done!';
    //     } else{
    //         return Redirect::route('home');
    //     }
    // }


    // public function insertGrupos()
    // {
    // 	if (Auth::user(1)) {
    //         $rows = DB::insert("UPDATE modelos SET grupo = 'BlueRay' WHERE modelo like 'BD%'");
    //         $rows = DB::insert("UPDATE modelos SET grupo = 'Docking' WHERE modelo like 'DA%'");
    //         $rows = DB::insert("UPDATE modelos SET grupo = 'DVD' WHERE modelo like 'DVD%'");
    //         $rows = DB::insert("UPDATE modelos SET grupo = 'HomeTheater' WHERE modelo like 'HT%'");
    //         $rows = DB::insert("UPDATE modelos SET grupo = 'HomeTheater' WHERE modelo like 'HW%'");
    //         $rows = DB::insert("UPDATE modelos SET grupo = 'MiniComp' WHERE modelo like 'MX%'");
    //         $rows = DB::insert("UPDATE modelos SET grupo = 'TV' WHERE modelo like 'PL%'");
    //         $rows = DB::insert("UPDATE modelos SET grupo = 'TV' WHERE modelo like 'PN%'");
    //         $rows = DB::insert("UPDATE modelos SET grupo = 'TV' WHERE modelo like 'UN%'");
    //         $rows = DB::insert("UPDATE modelos SET grupo = 'TV' WHERE modelo like 'UN%'");
    //         echo 'Done! \(ovo)/';
    //     } else {
    //          return Redirect::route('home');
    //     }
    // }

}
