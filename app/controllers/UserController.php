<?php

use Bluepoints\Forms\EditUserForm;
use Bluepoints\Registration\RegisterUserCommand;
use Bluepoints\Users\User;
use Laracasts\Commander\CommandBus;
use Bluepoints\Paises\Paises;



class UserController extends \BaseController {

    /**
     * @var SignInForm
     */
    private $editUserForm;

    public function __construct(EditUserForm $editUserForm) {
        $this->editUserForm = $editUserForm;
    }

    public function edit() {
        $paises = Paises::select('abvpais','nompais')->get();
        $paisesSelect[0] = trans('registro.pais_default');
        $user = Auth::user();
        foreach ($paises as $pais) {
            $paisesSelect[$pais->abvpais] = $pais->nompais;
        }
        return View::make('Registration.create')->withPaises($paisesSelect)->withUser($user);
    }

    public function update() {
        $this->editUserForm->validate(Input::all());
        $s = Auth::user()->update(Input::only('direccion','password','telefono','sucursal'));
        return Redirect::route('home')
                ->with('messages', trans('registro.cambio_data_exito'));
    }

}