<?php

use Bluepoints\Forms\ChangePasswordForm;

class PasswordController extends \BaseController {

    private $changePasswordForm;

    public function __construct(ChangePasswordForm $changePasswordForm) {
        $this->changePasswordForm = $changePasswordForm;
    }

    /**
     * @return mixed
     */
    public function reenviar()
    {
        return View::make('Sessions.remind');
    }

    /**
     * @return mixed
     */
    public function request()
    {
        switch ($response = Password::remind(Input::only('email'), function($message, $user) {
            $message->subject(trans('registro.email_recuperar_subject'));
        }))
        {
            case Password::INVALID_USER:
                return Redirect::route('login_path')->with('messages', trans('registro.recuperar_error_correo'));

            case Password::REMINDER_SENT:
                return Redirect::route('login_path')->with('messages', trans('registro.recuperar_correo_enviado'));
        }
    }
    public function reset($token)
    {
        return View::make('Sessions.reset')->with('token', $token);
    }

    /**
     * @return mixed
     */
    public function update()
    {
        $this->changePasswordForm->validate(Input::all());
        $credentials = Input::only(
            'email', 'password', 'password_confirmation', 'token'
        );

        $response = Password::reset($credentials, function($user, $password)
        {
            $user->password = $password;

            $user->save();
        });
        // cambiar los mensajes de respuesta
        switch ($response)
        {
            case Password::INVALID_PASSWORD:
                return Redirect::back()->with('messages', trans('registro.recuperar_error'))->withInput(Input::old());
            case Password::INVALID_TOKEN:
                return Redirect::back()->with('messages', trans('registro.recuperar_error'))->withInput(Input::old());
            case Password::INVALID_USER:
                return Redirect::back()->with('messages', trans('registro.recuperar_error'))->withInput(Input::old());
            case Password::PASSWORD_RESET:
                return Redirect::route('login_path')->with('messages', trans('registro.recuperar_exito'));
        }
    }
}