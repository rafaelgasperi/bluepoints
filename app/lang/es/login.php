<?php
	
	/**********************************************************
	*
	*   Lenguaje del Formulario de login en Español
	*
	* 	@author: Arturo Rossodivita
	*
	**********************************************************/


	return array(
		'email'					=> trans('registro.email'),
		'clave'					=> 'Contraseña',
		'registro'				=> 'Si no esta registrado, regístrese ',
		'aqui'					=> 'aquí.',
		'recuperar_clave'		=> 'Recuperar Clave',
		'iniciar_sesion'		=> 'Iniciar Sesión'
	);