<?php
	
	/**********************************************************
	*
	*   Lenguaje del Menu en Ingles
	*
	* 	@author: Alexandra Bergna woof woof
	*
	**********************************************************/


	return array(
		/*********************** Menú ************************/
		'titulo'		=> 'TABLA DE PUNTOS',
		'subtitulo'		=> 'Tabla de Puntos',
		'texto_ini'		=> '1. Por cada producto/modelo el vendedor recibirá una determinada cantidad de puntos,detallados a continuación:',
		'modelo'		=> 'Modelo',
		'puntos'		=> 'Puntos'

	);