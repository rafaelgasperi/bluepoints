<?php
	
	/**********************************************************
	*
	*   Lenguaje de Como participo en Español
	*
	* 	@author: Arturo Rossodivita
	*
	**********************************************************/


	return array(
		'titulo'			=> 'CÓMO PARTICIPO',
		'parrafo1'			=> 'Para participar y acumular puntos debes crear una cuenta en nuestro sistema y acumular los códigos por cada venta de productos SAMSUNG participantes.',
		'paso1'				=> '.Ingresa a tu cuenta. ',
		'paso2'				=> '.Registra el código secreto y completa los datos: Factura, Modelo de Producto​​, Cantidad y Fecha de la Factura.',
		'paso3'				=> '.El sistema te dirá el total de puntos ganado por el registro.',
		'paso4'				=> '.Con los puntos que acumules podrás reclamar premios.',
		'paso5'				=> '.Busca el premio que te guste y si tienes los puntos necesarios podrás solicitarlo.',
		'paso6'				=> '.Al solicitar el premio debitaremos los puntos de tu cuenta.',
		'paso7'				=> '.​Para reclamar el premio debes presentar el código activado y las facturas previamente registradas en la oficina de Samsung de tu país.',
		'en_cuenta'			=> 'Toma en cuenta:',
		'en_cuenta1'		=> '.Debes guardar todos los códigos que registres y las facturas registradas.',
		'en_cuenta2'		=> '.Podrás adquirir la cantidad de premios que desees, siempre y cuando tengas puntos suficientes.',
		'en_cuenta3'		=> '.Premios solicitados y que en un máximo de 15 días no sean reclamado serán anulados y los puntos se te acreditarán.',
		'en_cuenta4'		=> '.Una vez reclamado y retirado el premio no podrá devolverse.',
		'en_cuenta5'		=> '.Premios no serán entregados sino son presentadas las evidencias solicitada al momento de solicitar el premio.',
		'leer_reglamento'	=> '.Lee los términos y condiciones para conocer más de cómo funciona.​'
	);