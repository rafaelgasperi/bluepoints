<?php

	/**********************************************************
	*
	*   Lenguaje del Formulario de registro en Español
	*
	* 	@author: Arturo Rossodivita
	*
	**********************************************************/


	return array(
		'nombre'					=> 'Nombre',
		'nombre_placeholder'		=> 'Su Nombre...',
		'apellido'					=> 'Apellido',
		'apellido_placeholder'		=> 'Su Apellido...',
		'documento'					=> 'Documento de Identidad',
		'documento_indicaciones'	=> '(no usar caracteres especiales)',
		'documento_placeholder'		=> '012544512',
		'email'						=> 'E-Mail',
		'email_placeholder'			=> 'usuario@dominio.com',
		'email_confirmar'			=> 'Confirmar E-Mail',
		'clave'						=> 'Clave',
		'clave_indicaciones'		=> '(Minimo 6 caracteres, se aceptan caracteres especiales)',
		'clave_placeholder'			=> '',
		'clave_confirmar'			=> 'Confirmar Clave',
		'telefono'					=> 'Teléfono',
		'telefono_placeholder'		=> '041252487548',
		'tienda'					=> 'Nombre de Tienda y Sucursal',
		'tienda_placeholder'		=> 'Nombre de su tienda',
		'pais'						=> 'Pais',
		'pais_default'				=> 'Seleccione un País',
		'direccion'					=> 'Dirección',
		'direccion_placeholder'		=> 'Provincia, ciudad, sector, calle, residencia.',
		'terminos'					=> 'Acepto los Términos y Condiciones',
		'registrar'					=> 'REGISTRAR',
		'informacion'				=> 'Información:',
		'mensaje'					=> '<p>Revise su correo y confirme su cuenta haciendo CLIK en el link de confirmación.</p><p>Para Registrar sus Códigos debe confirmar su cuenta.</p>',
		'verificacion_titulo'		=> 'Activación de Código',
		'verificacion_mensaje'		=> 'Codigo Activado con exito',
		'verificacion_error'		=> 'Cuenta ya verificada o error en el link',
		'email_titulo'				=> 'Bienvenido/a',
		'email_mensaje1'			=> 'Hola',
		'email_mensaje2'			=> 'Necesitamos que valides tu cuenta haciendo click en este link:',
		'email_mensaje3'			=> 'Al validar tu cuenta podrás registrar los códigos que recibiste por la venta de productos de audio y video de Samsung para acumular puntos y canjear premios. Si no validas la cuenta, no podrás registrar los códigos y al cabo de 24 horas de recibido de este correo, se anulará tu cuenta.',
		'email_mensaje4'			=> 'Atentamente,',
		'email_mensaje5'			=> 'Samsung Latin America.',
		'email_subject'				=> 'Blue Points: Confirmación de cuenta',
		'email_recuperar_subject'	=> 'Blue Points: Confirmación de contraseña',
		'email_recuperar_saludo'	=> 'Hola',
		'email_recuperar_despedida'	=> 'Atentamente,',
		'email_recuperar_mensaje1'	=> 'Para restablecer tu contraseña favor has click en este link:',
		'recuperar_contrasena'		=> 'Recuperar Contraseña',
		'recuperar_error_correo'	=> 'Su Correo no está registrado.',
		'recuperar_correo_enviado'	=> 'Se ha enviado un email con las instrucciones para cambiar su contraseña',
		'recuperar_error'			=> 'Error en El Formulario: Minimo 6 caracteres, se aceptan caracteres especiales',
		'recuperar_exito'			=> 'Tu contraseña ha sido editada con éxito!',
		'contrasena'				=> 'Contraseña',
		'cambiar_contrasena'		=> 'Cambiar Contraseña',
		'cambio_data_exito'			=> 'Cambio de Datos realizado con exito'
	);