<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| as the size rules. Feel free to tweak each of these messages here.
	|
	*/

	"accepted"             => "El campo :attribute debe ser aceptado.",
	"active_url"           => "El campo :attribute no es una URL válida.",
	"after"                => "El campo :attribute debe de una fecha después de :date.",
	"alpha"                => "El campo :attribute sólo puede contener letras.",
	"alpha_dash"           => "El campo :attribute sólo puede contener letras, números, y guiones.",
	"alpha_num"            => "El campo :attribute sólo puede contener letras y números.",
	"array"                => "El campo :attribute debe ser un arreglo.",
	"before"               => "El campo :attribute debe ser una fecha antes de :date.",
	"between"              => array(
		"numeric" => "El campo :attribute debe estar entre :min y :max.",
		"file"    => "El campo :attribute debe estar entre :min y :max Kb.",
		"string"  => "El campo :attribute debe contener entre :min y :max caracteres.",
		"array"   => "El campo :attribute debe contener entre :min y :max objetos.",
	),
	"confirmed"            => "La confirmación de campo :attribute con coincide.",
	"date"                 => "El campo :attribute no es una fecha válida.",
	"date_format"          => "El campo :attribute does not match the format :format.",
	"different"            => "El campo :attribute y el campo :other deben ser diferentes.",
	"digits"               => "El campo :attribute debe contener :digits digitos.",
	"digits_between"       => "El campo :attribute debe contener entre :min y :max digitos.",
	"email"                => "El campo :attribute debe ser una dirección de correo válida.",
	"exists"               => "El campo :attribute seleccionado es invalido.",
	"image"                => "El campo :attribute debe ser una imagen.",
	"in"                   => "El campo :attribute seleccionado es invalido.",
	"integer"              => "El campo :attribute debe ser un número entero.",
	"ip"                   => "El campo :attribute debe ser una dirección IP válida.",
	"max"                  => array(
		"numeric" => "El campo :attribute no puede ser mayor a :max.",
		"file"    => "El campo :attribute no puede ser mayor a :max Kb.",
		"string"  => "El campo :attribute no puede contener mas de :max caracteres.",
		"array"   => "El campo :attribute no puede contener mas de :max objetos.",
	),
	"mimes"                => "El campo :attribute edbe ser un archivo de tipo: :values.",
	"min"                  => array(
		"numeric" => "El campo :attribute debe ser por lo menos :min.",
		"file"    => "El campo :attribute debe ser de por lo menos :min Kb.",
		"string"  => "El campo :attribute debe contener por lo menos :min caracteres.",
		"array"   => "El campo :attribute debe contener por lo menos :min objetos.",
	),
	"not_in"               => "El campo :attribute seleccionado es invalido.",
	"numeric"              => "El campo :attribute debe ser un número.",
	"regex"                => "El formato del campo :attribute es invalido.",
	"required"             => "El campo :attribute es requerido.",
	"required_if"          => "El campo :attribute es requerido cuando :other es :value.",
	"required_with"        => "El campo :attribute es requerido cuando :values esta presente.",
	"required_with_all"    => "El campo :attribute es requerido cuando :values estan presente.",
	"required_without"     => "El campo :attribute es requerido cuando :values no esta presente.",
	"required_without_all" => "El campo :attribute es requerido cuando ninguno de :values esta presente.",
	"same"                 => "El campo :attribute y :other deben coincidir.",
	"size"                 => array(
		"numeric" => "El campo :attribute debe ser de :size.",
		"file"    => "El campo :attribute debe ser de :size Kb.",
		"string"  => "El campo :attribute debe contener :size caracteres.",
		"array"   => "El campo :attribute debe contener :size objetos.",
	),
	"unique"               => "El campo :attribute ya existe en el sistema.",
	"url"                  => "El formato del campo :attribute es invalido.",
	"cantidad_disponible"  => "Cantidad mayor a la actual en existencia",
	"unique_for_user"  		=> "Factura Actualmente en el sistema",
    "required_en_array"     => "Selecciona un modelo",
	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => array(
		'attribute-name' => array(
			'rule-name' => 'custom-message',
		),
	),

	'custom' => array(
		'tos' => array(
			'required'	=> 'Debe Aceptar los <strong>Terminos y Condiciones</strong>'
		),
	),


	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => array(
		'nombre'			=> '<strong>Nombre</strong>',
		'apellido'			=> '<strong>Apellido</strong>',
		'identidad'			=> '<strong>Documento de Identidad</strong>',
		'email'				=> '<strong>E-mail</strong>',
		'password'			=> '<strong>Contraseña</strong>',
		'telefono'			=> '<strong>Teléfono</strong>',
		'pais'				=> '<strong>Pais</strong>',
		'sucursal_tienda'	=> '<strong>Tienda/Sucursal</strong>',
		'direccion'			=> '<strong>Dirección</strong>',
		'codigo'			=> '<strong>Código</strong>',
		'factura'			=> '<strong>Factura</strong>',
		'almacen'			=> '<strong>Sucursal</strong>',
		'fecha_venta'		=> '<strong>Fecha de Venta</strong>',
		'premio'			=> '<strong>Premios Disponibles</strong>',
		'total'				=> '<strong>Total de Puntos a Usar</strong>'
	),

);
