<?php
	
	/**********************************************************
	*
	*   Lenguaje del canje de Premios en Español
	*
	* 	@author: Arturo Rossodivita
	*
	**********************************************************/


	return array(
		'puntos_disponibles'				=> 'Puntos Disponibles',
		'premios_disponibles'				=> 'Premios Disponibles',
		'cantidad'							=> 'Cantidad del Premio',
		'puntos_requeridos'					=> 'Total de Puntos a Usar',
		'puntos'							=> 'Puntos',
		'submit'							=> 'CONFIRMA PREMIOS',
		'exito'								=> 'Transaccion realizada con exito!<br><br>',
		'email_subject'						=> 'Blue Points Nuevo Ganador',
		'email_titulo'						=> 'Nuevo Ganador',
		'email_nombre'						=> 'Nombre',
		'email_telefono'					=> 'Teléfono',
		'email'								=> 'Email',
		'email_pais'						=> 'País',
		'email_content'						=>  'Ha canjeado un :premio con el código de transacción :transaccion'
	);