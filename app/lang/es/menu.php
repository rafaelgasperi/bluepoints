<?php

	/**********************************************************
	*
	*   Lenguaje del Menu en Español
	*
	* 	@author: Arturo Rossodivita
	*
	**********************************************************/


	return array(
		'home'					=> 'HOME',
		'registro'				=> 'REGISTRARSE',
		'como_participar'		=> 'CÓMO PARTICIPAR',
		'premios'				=> 'PREMIOS',
		'reglas'				=> 'TÉRMINOS Y CONDICIONES',
		'cuenta'				=> 'MI CUENTA',
		'reg_codigo'			=> 'REGISTRAR CÓDIGO',
		'cambio'				=> 'CANJEAR PREMIO',
		'tablapuntos'           => 'TABLA DE PUNTOS',
		'historial'				=> 'VER HISTORIAL',
		'cerrar'				=> 'CERRAR SESIÓN'
	);