<?php

	/**********************************************************
	*
	*   Lenguaje del Registro Codigo en Español
	*
	* 	@author: Alexandra Bergna *wink wink*
	*
	**********************************************************/


	return array(


		'codigo_placeholder'		=> 'Código asociado a la factura',
		'factura_placeholder'		=> 'Número de la factura',
		'sucursal_placeholder'		=> 'Nombre de la tienda',
		'fecha_placeholder'			=> 'dd/mm/yyyy',
		'ingresar_codigo'			=> 'Ingrese Código',
		'ingresar_factura'			=> 'Ingrese Factura',
		'seleccionar_modelo'		=> 'Seleccione Modelo',
		'almacen'					=> 'Establecimiento de Venta',
		'fecha_factura'				=> 'Fecha de la Factura',
		'activar_codigo'			=> 'ACTIVAR CÓDIGO',
		'exito'						=> '!haz ganado :puntos puntos! <br><br> Sigue participando y acumula más puntos.',
		'add_models'                => 'AGREGAR OTRO',
		'delete_models'             => 'ELIMINAR MODELO',
	);