<?php

    /**********************************************************
    *
    *   Lenguaje del Mantenimiento Codigo en Español
    *
    *   @author: Alessandro Ceccarello.
    *
    **********************************************************/


    return array(

        'maintenance'           => 'En Mantenimiento',
    );