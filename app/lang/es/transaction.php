<?php

    return array(
         'codigo'      => 'Codigo',
         'factura'     => 'Factura',
         'modelo'      => 'Modelo',
         'cantidad'    => 'Cantidad',
         'pdf'         => 'Descargar PDF',
         'transaccion' => 'Codigo de Transaccion',
         'puntos'      => 'Puntos Usados',
         'premios'     => 'Premios Escogidos ',
         'presentar'   => 'Debe presentar los siguientes códigos y facturas'
    );

