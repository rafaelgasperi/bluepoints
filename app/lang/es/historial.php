<?php

	/**********************************************************
	*
	*   Lenguaje del Registro Codigo en Español
	*
	* 	@author: Alexandra Bergna *wink wink*
	*
	**********************************************************/


	return array(

		'detalle'			=> 'Detalles de Transacción',
		'fecha'				=> 'Fecha',
		'comentario'		=> 'Comentarios',
		'codigo'			=> 'Código',
		'factura'			=> 'Factura',
		'premio'			=> 'Premio',
		'ningun' 			=> 'No has realizado ningun canje de premio.',
		'descargar' 		=> 'DESCARGAR PDF',
		'desc_email' 		=> 'ENVIAR POR EMAIL',
		'detalle_tabla'     => 'Detalles',
		'intro_email'		=> '<br><p>Para reclamar el premio deberás presentar el código de activación, la factura de venta original o copia en la siguiente dirección:</p>',
		'end_email'			=> '<p>¡Gracias por su participación!</p>',
		
		'PA_email'			=> '​Dirección: Dirección: Calle 74 San Francisco, Edificio QH.<br /> Agencia: Clic Brand Marketing.<br>Contacto: Charlene Cruz.<br />Email: charlenecruz@clic.com.pa<br>Teléfono: (507) 270-0071<br>Contacto Samsung: l.saavedra@samsung.com / a.armijo@samsung.com',
		'CR_email'			=> 'Dirección: Centro Comercial La Rambla, Segundo Piso, Oficina Liftbrand.<br>Agencia: Liftbrand<br>Contacto:  Natalia Barrientos<br>Teléfono: (506) 2288 5454<br>E-mail: nbarrientos@liftbrand.net<br>Contacto Samsung: d.pacheco@samsung.com​',
		'SV_email'			=> 'Dirección: Edificio Comercial, Av. El Espino #77, Urb. Madreselva, Antiguo Cuscatlán, La Libertad, El Salvador.<br />Agencia: Eventing S.A de C.V<br />Contacto: Dulce María Vega Pacheco<br />Email: bluepoints@gcccontacto.com<br />Teléfono: (503) 2244-2222 Ext. 144<br>Contacto Samsung: portillo.b@samsung.com',
		'HON_email'			=> 'Dirección: Bo. Morazan Centro Comercial el Dorado, 5to piso telefono 2221 4034, Tegucigalpa Honduras<br />Agencia: Research and Planning<br />Contacto: Carlos Hernández<br />Email: chernandez@gcchn.net<br />Teléfono: (504) 2221-4034 / 9472-7148<br>Contacto Samsung: rodriguez.a@samsung.com',
		'RD_email'			=> 'Dirección: Calle Parabola #11 Esquina Elipse. Urbanización Fernandez. Santo Domingo, Distrito Nacional República Dominicana.<br />Agencia: Productiva.<br />Contacto: Sheila Bido<br />Email: sheila.bido@productiva.com.do<br />Teléfono: (809) 540-5517 Ext. 321<br>Contacto Samsung: matos.s@samsung.com / acosta.d@samsung.com',
		'NI_email'			=> 'Dirección: Rotonda El Periodista, 1 cuadra abajo, Managua, Nicaragua<br />Agencia: Publicidad Comercial<br />Contacto: Grethel Noguera<br />Email: ganadorespromobluepoints@gmail.com<br />Teléfono: (505) 2266-2575<br>Contacto Samsung: r.hanon@samsung.com',
		'T&T_email'			=> 'Dirección: #57 Carlos Street, Woodbrook. Port of Spain. Trinidad.<br />Agencia: Mango Media Caribbean Limited.<br />Contacto: Nicholas Campbell<br />Email: nicholas@mangomediacaribbean.com<br />Teléfono: 1-868-222-7939',
		'JAM_email'			=> 'Dirección: Marketing Plus Communication Ltd, Suite# 12B, Seymour Park, 2A Seymour Avenue, Kingston 10, Jamaica<br />Agencia: Marketing Plus<br />Contacto: Dionne Morris & Faun Fuller<br />Email: marketplus@cwjamaica.com<br />Teléfono: (876) 946-2027-8<br>Contacto Samsung: k.gardener@samsung.com',
		'ECU_email'			=> 'Dirección: Torres del Mall D, Piso 4; Recepcion de Regus.<br />Agencia: Marketing Plus<br />Contacto: Gabriela Muñoz<br />Email: gmunoz@misiva.com<br />Teléfono: 986940123<br>Contacto Samsung: castro.p@samsung.com / r.pasquel@samsung.com',
		
		'correo_msg'		=> 'Se ha enviado un email con el historial de forma exitosa',
		'anular_codigo'		=> 'Solicitar Anular Codigo',
		'anular_button'		=> 'Anular',
		'anular_placeholder'=> 'Motivo por el cual se esta anulando este codigo?',
		'anular_msg'		=> 'Codigo Anulado con exito, en 48 horas te estaremos contactando',
		'espera'		=> 'En Espera',
		'entregado'		=> 'Entregado',
		'rechazado'		=> 'Rechazado',
		'registro'		=> 'Codigo Activado',
		'regTab'		=> 'Registro de Códigos',
		'transTab'		=> 'Transacciones',

	);