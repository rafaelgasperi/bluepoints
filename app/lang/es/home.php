<?php

	/**********************************************************
	*
	*   Lenguaje del Home en Español
	*
	* 	@author: Arturo Rossodivita
	*
	**********************************************************/


	return array(
		'crear_cuenta'			=> 'Crea Tu Cuenta',
		'ingresar_codigo'		=> 'Ingresa el Código',
		'usar_puntos'			=> 'Usa Tus Puntos',
		'reclamar_premios'		=> 'Reclama Tus Premios',
		'premios'				=> 'Premios',
		'unidades'				=> 'Unidades Disponibles',
		'codigos_activos'		=> 'Códigos Activos',
		'puntos_acumulados'		=> 'Puntos Acumulados',
		'premios_canjeados'		=> 'Premios Canjeados',
		'premios_disponibles'	=> 'Premios disponibles',
		'espanol'				=> 'Español',
		'ingles'				=> 'Inglés',
		'cambiar_datos'			=> 'Cambiar mis Datos',
		'reference_images'		=> '*Las imagenes son solo referenciales',
	);