<?php 

return array(
	'registrar'					=> 'registrar',
	'verificar_registro'		=> 'registrar/verificar/{confirmationCode}',
	'registro_codigo'			=> 'registrar/codigo',
	'nueva_contrasena'			=> 'contrasena/nueva',
	'nueva_contrasena_token'	=> 'contrasena/nueva/{token}',
	'premios'					=> '/premios',
	'cambiar_premio'			=> '/premio/cambiar',
	'premio_transaccion'		=> '/premio/transaccion/{transaccion_id}',
	'como_participar'			=> '/como-participar',
	'reglamentos'				=> '/reglamentos',
	'tabla_puntos'				=> '/tabla_puntos',
	'historial'					=> '/historial',
	'getcode'					=> '/getcode',
	'usepoints'					=> '/usepoints',
	'obtener_modelos'			=> '/modelos',
	'obtener_premios'			=> '/premios/{id}/puntos',
	'getprize'					=> '/getprize',
	'img_premios'				=> '/premios/pais/{id}',
	'pdf_test'					=> '/pdf-test/{transaccion_id}',
	'transaccion_pdf'			=> '/premio/transaccion/{transaccion_id}/pdf',
	'historial_pdf'				=> '/historial/pdf',
	'historial_email'			=> '/historial/email'
);