<?php

	/**********************************************************
	*
	*   Lenguaje del Formulario de registro en Ingles
	*
	* 	@author: Arturo Rossodivita
	*
	**********************************************************/


	return array(
		'nombre'					=> 'Name',
		'nombre_placeholder'		=> 'Your Name...',
		'apellido'					=> 'Last Name',
		'apellido_placeholder'		=> 'Your Last Name...',
		'documento'					=> 'ID Number',
		'documento_indicaciones'	=> '(dont use special characters)',
		'documento_placeholder'		=> '012544512',
		'email'						=> 'E-Mail',
		'email_placeholder'			=> 'user@domain.com',
		'email_confirmar'			=> 'Rewrite E-Mail',
		'clave'						=> 'Password',
		'clave_indicaciones'		=> '(Minimum 6 characters, special characters are accepted)',
		'clave_placeholder'			=> '',
		'clave_confirmar'			=> 'Rewrite Password',
		'telefono'					=> 'Phone Number',
		'telefono_placeholder'		=> '041252487548',
		'tienda'					=> 'Store - Branch Office',
		'tienda_placeholder'		=> 'Your Store',
		'pais'						=> 'Country',
		'pais_default'				=> 'Select Country',
		'direccion'					=> 'Address',
		'direccion_placeholder'		=> 'State, city, street, residence.',
		'terminos'					=> 'I Accept the Terms & Conditions',
		'registrar'					=> 'REGISTER',
		'informacion'				=> 'Information:',
		'mensaje'					=> '<p>Check your email and confirm your account by CLIK on confirmation link.</p> <p>To Register your codes must confirm your account.</p>',
		'verificacion_titulo'		=> 'Code Activation',
		'verificacion_mensaje'		=> 'Successful Activation Code',
		'verificacion_error'		=> 'Account it has already verified or error in the link',
		'email_titulo'				=> 'Welcome',
		'email_mensaje1'			=> 'Hi',
		'email_mensaje2'			=> 'We need to validate your account by clicking on this link:',
		'email_mensaje3'			=> 'When you validate your account, you can register the hidden codes to participate and redeem prizes. If your account is not valid, you will not be able to register the hidden codes and after 24 hours of receipt this mail, your account will be canceled.',
		'email_mensaje4'			=> 'Thank you,',
		'email_mensaje5'			=> 'Samsung Latin America.',
		'email_subject'				=> 'Blue Points: Account Confirmation',
		'email_recuperar_subject'	=> 'Blue Points: Password Reset',
		'email_recuperar_saludo'	=> 'Hello',
		'email_recuperar_despedida'	=> 'Best Regards,',
		'email_recuperar_mensaje1'	=> 'To reset your password visit this link:',
		'recuperar_contrasena'		=> 'Reset Password',
		'recuperar_error_correo'	=> 'Your E-Mail is not registered',
		'recuperar_correo_enviado'	=> 'Sent an email with instructions to reset your password',
		'recuperar_error'			=> 'Form Error: Minimum 6 characters, special characters are accepted',
		'recuperar_exito'			=> 'Your password has been reset successfully!',
		'contrasena'				=> 'Password',
		'cambiar_contrasena'		=> 'Change Password',
		'cambio_data_exito'			=> 'Change Data successfully'
	);