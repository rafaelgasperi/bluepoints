<?php

	/**********************************************************
	*
	*   Lenguaje del Menu en Ingles
	*
	* 	@author: Arturo Rossodivita
	*
	**********************************************************/


	return array(
		/*********************** Menú ************************/
		'home'					=> 'HOME',
		'registro'				=> 'REGISTER',
		'como_participar'		=> 'HOW TO PARTICIPATE?',
		'premios'				=> 'PRIZES',
		'reglas'				=> 'TERMS AND CONDITIONS',
		'cuenta'				=> 'MY ACCOUNT',
		'reg_codigo'			=> 'REGISTER CODE',
		'cambio'				=> 'REDEEM PRIZES',
		'tablapuntos'			=> 'POINTS TABLE',
		'historial'				=> 'VIEW HISTORY',
		'cerrar'				=> 'LOGOUT'
	);