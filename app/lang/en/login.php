<?php
	
	/**********************************************************
	*
	*   Lenguaje del Formulario de login en Ingles
	*
	* 	@author: Arturo Rossodivita
	*
	**********************************************************/


	return array(
		'email'					=> trans('registro.email'),
		'clave'					=> 'Password',
		'registro'				=> 'If you are not registered, register ',
		'aqui'					=> 'here.',
		'recuperar_clave'		=> 'Retrieve Password',
		'iniciar_sesion'		=> 'LOG IN'
	);