<?php
	
	/**********************************************************
	*
	*   Lenguaje de Como participo en Ingles
	*
	* 	@author: Arturo Rossodivita
	*
	**********************************************************/


	return array(
		'titulo'			=> 'How to Participate',
		'parrafo1'			=> 'To Participate you must register and collect the codes for every SAMSUNG participating product sale.',
		'paso1'				=> '.Log In.',
		'paso2'				=> '.Register the secret code and complete the following information: Invoice Number, Product Model, Amount and Invoice date.',
		'paso3'				=> '.The system will let you know the points you’ve earned for the registry.',
		'paso4'				=> '.With the points you collect you will be able to redeem prizes.',
		'paso5'				=> '.Look for a prize you like, if you have the amount of points necessary you’ll be able to redeem it.',
		'paso6'				=> '.When you choose a prize we’ll deduct the points from your account.',
		'paso7'				=> '.You will receive a redemption code that you have to present with the invoices and the secret codes at the Samsung office of your country.',
		'en_cuenta'			=> 'To consider:',
		'en_cuenta1'		=> '.You must save all the codes and invoices that you register.',
		'en_cuenta2'		=> '.You can choose the amount of prizes you like, as long as you have enough points.',
		'en_cuenta3'		=> '.If you choose a prize and don’t redeem it within 15 days the request will be annulled and your points will be returned.',
		'en_cuenta4'		=> '.Once you choose and claim your prize you will not be able to return it.',
		'en_cuenta5'		=> '.Prizes will not be delivered if the evidence requested is not presented.',
		'leer_reglamento'	=> '.Read the BLUE POINT rules to learn more.'
	);