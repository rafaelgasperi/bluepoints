<?php

	/**********************************************************
	*
	*   Lenguaje del Registro Codigo en Ingles
	*
	* 	@author: Alexandra Bergna *wink wink*
	*
	**********************************************************/


	return array(

		'codigo_placeholder'		=> 'Invoice code',
		'factura_placeholder'		=> 'Invoice number',
		'sucursal_placeholder'		=> "Store's name",
		'fecha_placeholder'			=> 'dd/mm/yyyy',
		'ingresar_codigo'			=> 'Enter Code',
		'ingresar_factura'			=> 'Enter Invoice',
		'seleccionar_modelo'		=> 'Select Model',
		'almacen'					=> 'Store',
		'fecha_factura'				=> 'Invoice Date',
		'activar_codigo'			=> 'ACTIVATE CODE',
		'exito'						=> 'you have won :puntos points! <br><br> Stay involved and accumulate more points.',
		'add_models'                => 'ADD ANOTHER',
		'delete_models'             => 'DELETE MODEL',
	);