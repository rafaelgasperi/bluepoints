<?php
	
	/**********************************************************
	*
	*   Lenguaje del canje de Premios en Ingles
	*
	* 	@author: Arturo Rossodivita
	*
	**********************************************************/


	return array(
		'puntos_disponibles'				=> 'Points Available',
		'premios_disponibles'				=> 'Prizes Available',
		'cantidad'							=> 'Prizes Amount',
		'puntos_requeridos'					=> 'Points to use',
		'puntos'							=> 'Points',
		'submit'							=> 'CONFIRM PRIZES',
		'exito'								=> 'Transaction completed successfully!<br><br>',
		'email_subject'						=> 'Blue Points New Winner',
		'email_titulo'						=> 'New Winner',
		'email_nombre'						=> 'Name',
		'email_telefono'					=> 'Phone',
		'email'								=> 'Email',
		'email_pais'						=> 'Country',
		'email_content'						=>  'Has redeemed a :premio with transaction code :transaccion and invoice :invoice'
	);