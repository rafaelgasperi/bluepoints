<?php 

return array(
	'registrar'					=> 'register',
	'insert_grupos'				=> 'insert/grupos',
	'verificar_registro'		=> 'register/verify/{confirmationCode}',
	'registro_codigo'			=> 'register/code',
	'nueva_contrasena'			=> 'password/new',
	'nueva_contrasena_token'	=> 'password/new/{token}',
	'premios'					=> '/prizes',
	'cambiar_premio'			=> '/prizes/change',
	'premio_transaccion'		=> '/prizes/transaction/{transaccion_id}',
	'como_participar'			=> '/how-participate',
	'reglamentos'				=> '/rules',
	'tabla_puntos'				=> '/point_table',
	'historial'					=> '/history',
	'getcode'					=> '/getcode',
	'usepoints'					=> '/usepoints',
	'obtener_modelos'			=> '/models',
	'obtener_premios'			=> '/prizes/{id}/points',
	'getprize'					=> '/getprize',
	'img_premios'				=> '/prizes/country/{id}',
	'pdf_test'					=> '/pdf-test/{transaccion_id}',
	'transaccion_pdf'			=> '/prizes/transaction/{transaccion_id}/pdf',
	'historial_pdf'				=> '/history/pdf',
	'historial_email'			=> '/history/email'
);