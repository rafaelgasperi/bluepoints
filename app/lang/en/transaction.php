<?php

    return array(
        'codigo'      => 'Code',
        'factura'     => 'Invoice',
        'modelo'      => 'Model',
        'cantidad'    => 'Quantity',
        'pdf'         => 'Download PDF',
        'transaccion' => 'Transaction Code',
        'puntos'      => 'Points Used',
        'premios'     => 'Prize Chosen',
        'presentar'   => 'You must present the following codes and invoices'
    );
