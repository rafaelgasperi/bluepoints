<?php

	/**********************************************************
	*
	*   Lenguaje del Home en Español
	*
	* 	@author: Arturo Rossodivita
	*
	**********************************************************/


	return array(
		'crear_cuenta'			=> 'CREATE YOUR ACCOUNT',
		'ingresar_codigo'		=> 'ENTER THE CODE',
		'usar_puntos'			=> 'USE YOUR POINTS',
		'reclamar_premios'		=> 'CLAIM YOUR PRIZE',
		'premios'				=> 'PRIZES',
		'unidades'				=> 'Units Available',
		'codigos_activos'		=> 'Activated Codes',
		'puntos_acumulados'		=> 'Points',
		'premios_canjeados'		=> 'Redeemed Prizes',
		'premios_disponibles'	=> 'Available Prizes',
		'espanol'				=> 'Spanish',
		'ingles'				=> 'English',
		'cambiar_datos'			=> 'Change my Data',
		'reference_images'		=> '*Reference Images',
	);