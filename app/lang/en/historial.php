<?php

	/**********************************************************
	*
	*   Lenguaje del Registro Codigo en Español
	*
	* 	@author: Alexandra Bergna *wink wink*
	*
	**********************************************************/


	return array(

		'detalle'			=> 'Details of Transaction',
		'fecha'				=> 'Date',
		'comentario'		=> 'Comments',
		'codigo'			=> 'Code',
		'factura'			=> 'Invoice',
		'premio'			=> 'Prize',
		'ningun' 			=> "You haven't redeemed any prizes.",
		'descargar' 		=> 'DOWNLOAD PDF',
		'desc_email' 		=> 'SEND TO EMAIL',
		'detalle_tabla'     => 'Details',
		'intro_email'		=> '<br><p>To claim the prize, you must submit the hidden code and the printed bill to the following address:</p>',
		'end_email'			=> '<p>Thank you.</p>',
		
		'PA_email'			=> '​Address: Samsung Electronics, Torre las Américas, Torre C, Piso 23, Ciudad de Panamá.<br>Contact: Ania Armijo / Lisbeth Saavedra<br>Phone: (507) 306-2800',
		'CR_email'			=> 'Address: Centro Comercial La Rambla, Segundo Piso, Oficina Liftbrand.<br>Agency: Liftbrand<br>Contact:  Evelyn Sandi Aguirre<br>Phone: (506) 2288 5454<br>E-mail: vendedores@liftbrand.com<br>',
		'SV_email'			=> 'Address: Samsung Electronics, Edificio Avante, Local 3-12 Calle llama del Bosque Poniente pasaje "S", Antiguo Cuscatlán, La libertad, El Salvador. <br>Contact: Billy Portillo <br>Phone: (503) 22460241',
		'HON_email'			=> 'Address: Samsung Electronics, Edificio Corporativo los Próceres. Piso 8, No 802, entre Ave. La Paz y Boulevard los Próceres. Tegucigalpa, Honduras.<br>Contact: Roberto Salgado<br>Phones: (504) 22681251 / 22681279',
		'RD_email'			=> 'Address: Calle Gardenia # 9 , Sector Gala.<br>Contact: Omar Espinoza / Patricia Marinez<br>Phones: (809) 338-2600',
		'NI_email'			=> 'Address: Samsung Electronics, Edificio El Centro 2, Piso 3, Local 302-A, de Rotonda El Periodista 600 mts. al Norte<br>Contact: Raquel Hannon<br>Phone: (505) 2251-2378',
		'T&T_email'			=> 'Address:  #57 Carlos Street, Woodbrook. Port of Spain. Trinidad.<br>Contact: Nicholas Campbell<br>Email: nicholas@mangomediacaribbean.com<br>Phone: 1-868-222-7939<br>Samsung Contact: g.whyte@samsung.com',
		'JAM_email'			=> 'Address: Marketing Plus Communication Ltd, Suite# 12B, Seymour Park, 2A Seymour Avenue, Kingston 10, Jamaica.<br>Contact: Dionne Morris & Faun Fuller<br>Phone: (876) 946-2027-8<br>Samsung Contact: k.gardener@samsung.com',
		'ECU_email'			=> 'Address: <br>Contact:<br>Phone:',

		'correo_msg'		=> 'An email has been sent with the history information succesfully',
		'anular_codigo'		=> 'Nullify a code',
		'anular_button'		=> 'Nullify',
		'anular_placeholder'=> 'Why are you nullifying this code?',
		'anular_msg'		=> 'Code nullified with success, in 48 hours we\'ll contact you',
		'espera'		=> 'Waiting',
		'entregado'		=> 'Delivered',
		'rechazado'		=> 'Rejected',
		'registro'		=> 'Code Activated',
		'regTab'		=> 'Codes Activated',
		'transTab'		=> 'Transactions',
	);