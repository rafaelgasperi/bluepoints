<?php
	
	/**********************************************************
	*
	*   Lenguaje del Menu en Ingles
	*
	* 	@author: Alexandra Bergna woof woof
	*
	**********************************************************/


	return array(
		/*********************** Menú ************************/
		'titulo'		=> 'POINT CHART',
		'subtitulo'		=> 'Point Chart',
		'texto_ini'		=> '1. For every product/model, the seller will receive a certain amount of points, listed below:',
		'modelo'		=> 'Model',
		'puntos'		=> 'Points'

	);