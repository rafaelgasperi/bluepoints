<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


$languages = array('en', 'es');
$prefix = '';
if (in_array(Request::segment(1), $languages))
{
    \App::setLocale(Request::segment(1));
    $prefix = Request::segment(1);
}

Validator::extend('required_en_array', function($field,$value,$parameters){
    $valor = true;
        foreach ($value as $v) {
                  if($v != "") {
                              $valor = true;
                                    } else {
                                                return false;
                                                      }
                      }
        return $valor;
});



Route::group(array('prefix' => $prefix), function () {

      Route::get('/',[
        'as' => 'home',
        'uses'=> 'HomeController@showWelcome'
      ]);

      /**
       * Registrar Persona
       */
      Route::get(trans('rutas.registrar'), [
        'as' => 'registro_path',
        'uses' => 'RegistrationController@create'
      ]);

      Route::post(trans('rutas.registrar'), [
          'before' => 'csrf',
          'as' => 'registro_path',
          'uses' => 'RegistrationController@store'
      ]);

      Route::get(trans('rutas.verificar_registro'), [
        'as' => 'verificar_registro_path',
        'uses' => 'RegistrationController@verificar'
      ]);

      /**
       * Login
       */
      Route::get('login', [
          'as' => 'login_path',
          'uses' => 'SessionsController@create'
      ]);



      Route::post('login', [
          'before' => 'csrf',
          'as' => 'login_path_post',
          'uses' => 'SessionsController@store'
      ]);

      Route::get('logout', [
          'as' => 'logout_path',
          'uses' => 'SessionsController@destroy'
      ]);

      /**
       * Registrar Codigo
       */
      Route::get(trans('rutas.registro_codigo'), [
        'as' => 'registro_codigo_path',
        'uses' => 'RegistrarCodigosController@create'
      ]);

      Route::post(trans('rutas.registro_codigo'), [
          'before' => 'csrf',
          'as' => 'registro_codigo_path',
          'uses' => 'RegistrarCodigosController@store'
      ]);

      Route::get(trans('rutas.nueva_contrasena'), array(
          'as' => 'password_remind_path',
          'uses' => 'PasswordController@reenviar'
      ));

      Route::post(trans('rutas.nueva_contrasena'), array(
          'before' => 'csrf',
          'as' => 'password_remind_path',
          'uses' => 'PasswordController@request'
      ));

      Route::get(trans('rutas.nueva_contrasena_token'), array(
          'uses' => 'PasswordController@reset',
          'as' => 'password.reset'
      ));

      Route::post(trans('rutas.nueva_contrasena_token'), array(
          'uses' => 'PasswordController@update',
          'as' => 'password.update'
      ));


      /**
       * Mostrar Premios
      */

      Route::get(trans('rutas.premios'), [
          'as'    => 'premios_path',
          'uses'  => 'PremiosController@showPremios'
      ]);

      Route::get(trans('rutas.cambiar_premio'), [
          'as'    => 'cambiar_premio_path',
          'uses'  => 'PremiosController@redeem'
      ]);

      Route::post(trans('rutas.cambiar_premio'), [
          'before' => 'csrf',
          'as'     => 'cambiar_premio_path',
          'uses'   => 'PremiosController@store'
      ]);

      Route::get(trans('rutas.premio_transaccion'), [
          'as'    => 'premio_transaccion',
          'uses'  => 'PremiosController@transaction'
      ]);

      Route::get(trans('rutas.como_participar'), [
          'before' => 'guest',
          'as'    => 'como_participar_path',
          'uses'  => 'HomeController@showComoParticipar'
      ]);

      /**
      * Reglamentos
      */

      Route::get(trans('rutas.reglamentos'), [
          'as'    => 'reglamentos_path',
          'uses'  => 'HomeController@showReglamentos'
      ]);


      /**
      * Historial
      */

      Route::get(trans('rutas.historial'), [
          'as'    => 'historial_path',
          'uses'  => 'HistorialController@showHistorial'
      ]);


      /**
      * Menu Buttons (Alexa Code)
      */


      Route::get(trans('rutas.obtener_modelos'), [
          'as'    => 'obtener_modelos',
          'uses'  => 'RegistrarCodigosController@modelos'
      ]);

      Route::get(trans('rutas.obtener_premios'), [
          'as'    => 'obtener_premios',
          'uses'  => 'PremiosController@puntosDePremio'
      ]);




      Route::get(trans('rutas.img_premios'), [
          'as'    => 'img_premios',
          'uses'  => 'PremiosController@showPremiosPais'
      ]);

      Route::get(trans('rutas.pdf_test'), [
        'as' => 'pdf_test_route',
        'uses' => 'PremiosController@makePdf'
      ]);

      Route::get(trans('rutas.transaccion_pdf'), [
        'as' => 'transaccion_pdf',
        'uses' => 'PremiosController@pdfBlade'
      ]);

      Route::get(trans('rutas.historial_pdf'), [
        'as' => 'historial_pdf',
        'uses' => 'HistorialController@getPdfHistorial'
      ]);

      Route::get(trans('rutas.historial_email'), [
        'as' => 'historial_email',
        'uses' => 'HistorialController@getEmailHistorial'
      ]);

      Route::get(trans('rutas.tabla_puntos'), [
        'as' => 'tabla_puntos_path',
        'before' => 'auth',
        'uses' => 'HomeController@showTablaPuntos'
      ]);


      /**
       * Erase DB
       */

      // Route::get('/erase', [
      //     'as'    => 'erase_path',
      //     'uses'  => 'HomeController@erase'
      // ]);

      Route::get('/pdf-blade/{transaccion_id}', [
        'as' => 'pdf_test_route',
        'uses' => 'PremiosController@pdfBlade'
      ]);

      Route::get('/user/edit', [
        'as' => 'edit_user',
        'uses' => 'UserController@edit'
      ]);

      Route::post('/user/edit', [
        'as' => 'edit_user',
        'uses' => 'UserController@update'
      ]);

});

  /**
   * Validations
   */

  Validator::extend('puntos_disponibles', function($field,$value,$parameters,$validator){
    return (Auth::user()->registro()->sum('puntos_restantes') >= calcularPuntosPremios($validator->getData()['premio'], $validator->getData()['cantidad']));
  });

  Validator::extend('unique_for_user', function($field,$value,$parameters,$validator){
    //retorna true si la factura es igual a una que ya tiene el usuario registrado
    $registros = Registro::whereUser_id(Auth::user()->id)->get();
    $facturas = [];
    foreach ($registros as $registro) {
      $facturas[] = $registro->factura;
      if ($registro->factura == $validator->getData()['factura']) {
        return false;
      }
    }
    return true;
  });

  Validator::extend('diferente_en_array', function($field,$value,$parameters){
    foreach ($value as $v) {
        $v = array_shift($value);
        foreach ($value as $v2) {
            if ($v == $v2) {
                return false;
            }
        }
    }
    return true;
  });

  Validator::extend('numeric_en_array', function($field,$value,$parameters){
    foreach ($value as $v) {
      if(preg_match('/^[+]?\d+([.]\d+)?$/', $v)) {
        return true;
      }
    }
    return false;
  });

  Validator::extend('positive', function($field,$value,$parameters){
    return preg_match('/^[+]?\d+$/', $value);
  });

  Validator::extend('cantidad_disponible', function($field,$value,$parameters,$validator){
    return (int)Premios::find($validator->getData()['premio'])->disponible >= $value;
  });

// Route::get('insert/grupos', [
//     'as' => 'insert_grupos',
//     'uses' => 'HomeController@insertGrupos'
//   ]);

Route::post('anular/codigo' , [
  'as' => 'anular_codigo',
  'uses' => 'HistorialController@anular'
]);

// Fix de Nicaragua
Route::get('actualizar-nicaragua', function () {
    // Econtrar a todos los nicaraguenses
    $nicaragua = \Bluepoints\Users\User::wherePais('NI')->get();

    $registros = [];
    $modelos = [];
    $i = 0;

    // Conseguir cada factura registrada
    foreach ($nicaragua as $nicaraguense) {
      $registro = \Bluepoints\Registros\Registros::whereUser_id($nicaraguense->id)->get();
      if (count($registro) > 0 ) $registros[] = $registro;
    }

    // Conseguir todos los modelos registrados en cada factura
    foreach ($registros as $registroDeUsuario) {
      foreach($registroDeUsuario as $registro) {
        $modelo = Modelo_Registro::whereRegistro_id($registro->id)->get();
        if (count($modelo) > 0 ) $modelos[] = $modelo;
      }
    }
    $puntos = [];

    // Calcular los nuevos puntos de cada factura segun su modelo
    foreach ($modelos as $modelosDeRegistro) {
      $punto = 0;
      foreach($modelosDeRegistro as $modelo) {
        $pts = \Bluepoints\Modelos\Modelos::findOrFail($modelo->modelo_id)->nicaragua;
        $punto += ($pts * (int) $modelo->cantidad);
      }
      $puntos[] = $punto;
    }
    // actualizar los puntos de la gente
    $i = 0;
    foreach($registros as $registroDeUsuario) {
      foreach($registroDeUsuario as $registro) {
          $registro->puntos_restantes = $puntos[$i++];
          $registro->save();
      }
    }
    return 'done';
});

// Fix de honduras
// Route::get('actualizar-honduras', function () {
//     // Econtrar a todos los nicaraguenses
//     $honduras = \Bluepoints\Users\User::wherePais('HON')->get();

//     $registros = [];
//     $modelos = [];
//     $i = 0;

//     // Conseguir cada factura registrada
//     foreach ($honduras as $hondureno) {
//       $registro = \Bluepoints\Registros\Registros::whereUser_id($hondureno->id)->get();
//       if (count($registro) > 0 ) $registros[] = $registro;
//     }

//     // Conseguir todos los modelos registrados en cada factura
//     foreach ($registros as $registroDeUsuario) {
//       foreach($registroDeUsuario as $registro) {
//         $modelo = Modelo_Registro::whereRegistro_id($registro->id)->get();
//         if (count($modelo) > 0 ) $modelos[] = $modelo;
//       }
//     }
//     $puntos = [];

//     // Calcular los nuevos puntos de cada factura segun su modelo
//     foreach ($modelos as $modelosDeRegistro) {
//       $punto = 0;
//       foreach($modelosDeRegistro as $modelo) {
//         $pts = \Bluepoints\Modelos\Modelos::findOrFail($modelo->modelo_id)->honduras;
//         $punto += ($pts * (int) $modelo->cantidad);
//       }
//       $puntos[] = $punto;
//     }
//     // actualizar los puntos de la gente
//     $i = 0;
//     foreach($registros as $registroDeUsuario) {
//       foreach($registroDeUsuario as $registro) {
//         $registro->puntos_restantes = $puntos[$i++];
//         $registro->save();
//       }
//     }
//     return 'done';
// });

Route::get('error-test', function () {
    return $prueba;
});

Route::get('eliminar-modelo', function() {
  return trans('registrocodigo.delete_models');
});
