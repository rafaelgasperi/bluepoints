/*
    See dev dependencies https://gist.github.com/isimmons/8927890
    Compiles sass to compressed css with autoprefixing
    Compiles coffee to javascript
    Livereloads on changes to coffee, sass, and blade templates
    Runs PHPUnit tests
    Watches sass, coffee, blade, and phpunit
    Default tasks sass, coffee, phpunit, watch
*/

var gulp = require('gulp');
var gutil = require('gulp-util');
var notify = require('gulp-notify');
var sass = require('gulp-ruby-sass');
var autoprefix = require('gulp-autoprefixer');
// var fs = require('fs'); //only used for icon file with growlNotifier


// livereload
var livereload = require('gulp-livereload');
var server = livereload(1337);

// var lr = require('tiny-lr');
// var server = lr();

//uncomment for growl notify for windows users
//Specify custom icon by passing object to growl() { icon: fs.readFileSync('path_to_icon_file') }
//var growl = ('gulp-notify-growl');
//var growlNotifier = growl();

//CSS directories
var sassDir = 'app/assets/sass';
var targetCSSDir = 'public/assets/css';

//javascript directories
var coffeeDir = 'app/assets/coffee';
var targetJSDir = 'public/js';

// blade directory
var bladeDir = 'app/views';

// Tasks
/* sass compile */
gulp.task('sass', function() {
    gulp.src(sassDir + '/main.scss')
        .pipe(sass())
        .pipe(gulp.dest(targetCSSDir))
        .pipe(autoprefixer('last 10 versions'))
        .pipe(notify('CSS compiled, prefixed, and minified.'));

        //growlNotifier for windows
        //.pipe(notify({title: 'CSS Compiled', message: 'compiled, prefixed, and minified.', notifier: growlNotifier}));
});



/* Blade Templates */
// gulp.task('blade', function() {
//     return gulp.src(bladeDir + '/**/*.blade.php')
//         .pipe(livereload(server));
// });

/* PHPUnit */


/* Watcher */
// var livereload = require('gulp-livereload');

gulp.task('watch', function () {
    // var server = livereload(1337);
    gulp.watch(sassDir+'**/*.scss', ['sass']);
    // gulp.watch('**/*.php').on('change', function(file) {
    //   server.changed(file.path);
    // });
});


/* Default Task */
gulp.task('default', ['watch']);